CONFIG(release, debug|release){
    message(Release)
    BUILD_TYPE = release
}else{
    message(Debug)
    BUILD_TYPE = debug
}
BUILD_DIR = $$PWD/build
DEST_INCLUDE_DIR = $$PWD/include
unix{
    ARCH_DIR       = $${OUT_PWD}/unix
    ARCH_TYPE      = unix
    macx{
        ARCH_DIR       = $${OUT_PWD}/macx
        ARCH_TYPE      = macx
    }
    linux{
        !contains(QT_ARCH, x86_64){
            message("Compiling for 32bit system")
            ARCH_DIR       = $${OUT_PWD}/linux32
            ARCH_TYPE      = linux32
        }else{
            message("Compiling for 64bit system")
            ARCH_DIR       = $${OUT_PWD}/linux64
            ARCH_TYPE      = linux64
        }
    }
}
win32 {
    ARCH_DIR       = $${OUT_PWD}/win32
    ARCH_TYPE      = win32
}

DEST_LIBS = $${BUILD_DIR}
DEFINES+=HAVE_QT5
contains(QT,uitools){
     message(uitools)
     DEFINES += HAVE_UI_LOADER
}

