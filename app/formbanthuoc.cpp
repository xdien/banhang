#include "formbanthuoc.h"
#include "ui_formbanthuoc.h"

FormBanThuoc::FormBanThuoc(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::FormBanThuoc)
{
    ui->setupUi(this);
    queryModelKetQua = new QSqlQueryModel();
    dsThuocChon = new QStandardItemModel();
    cbModelNhom = new QSqlQueryModel();
    cbNhomHangModel = new QSqlQueryModel();
    gia= new Gia();
    ui->treeViewKetQua->setModel(queryModelKetQua);
    ui->tableViewThuocChon->setModel(dsThuocChon);
    ui->comboBoxNhomKh->setModel(cbModelNhom);
    ui->tableViewThuocChon->verticalHeader()->setContextMenuPolicy(Qt::CustomContextMenu);
    cbNhomHangModel->setQuery("SELECT NHOM_HANG_HOA_ID FROM NHOM_HANG_HOA");
    ui->comboBoxNhomHang->setModel(cbNhomHangModel);
    connect(ui->tableViewThuocChon->verticalHeader(),SIGNAL(customContextMenuRequested(QPoint)),
            this,SLOT(customMenuRequested(QPoint)));
    lamMoiNhomKH();
    lamMoiBangketQua();
}

FormBanThuoc::~FormBanThuoc()
{
    delete ui;
    delete dsThuocChon;
    delete queryModelKetQua;
    delete gia;
    delete cbNhomHangModel;
}

void FormBanThuoc::on_lineEditTim_textChanged(const QString &arg1)
{
    if(ui->checkBoxLocKq->isChecked()){
        queryModelKetQua->setQuery("SELECT HANGHOAID,TEN_HANG_HOA,SO_DU FROM HANG_HOA where NHOM_HANG_HOA_ID = '"+cbNhomHangModel->index(ui->comboBoxNhomHang->currentIndex(),0).data().toString()+"' and TEN_HANG_HOA like '%"+arg1+"%'");
    }else{
        queryModelKetQua->setQuery("SELECT HANGHOAID,TEN_HANG_HOA,SO_DU FROM HANG_HOA where TEN_HANG_HOA like '%"+arg1+"%'");
    }
}


void FormBanThuoc::on_pushButton_2_clicked()
{
    /*ma thuoc, ten thuoc, so lo,soluong*/
    //Them tong so tien,  tim ma hang co ton tai hay chua
    if(dsThuocChon->findItems(ui->lineEditMaHang->text(),Qt::MatchExactly,0).count() <= 0){
        int tongtien = ui->lineEditTongDonGia->text().toInt(0) + ui->lineEditTongDonThuoc->text().toInt(0);
        ui->lineEditTongDonThuoc->setText(QString::number(tongtien));
        dsThuocChon->appendRow(
                    add(ui->lineEditMaHang->text(),ui->lineEditTenHang->text(),ui->spinBoxSoLuong->text(),ui->lineEditDonGia->text(),ui->lineEditTongDonGia->text()));
        if(dsThuocChon->rowCount() ==1){
            dsThuocChon->setHeaderData(0,Qt::Horizontal,QObject::tr("Ma Hang"));
            dsThuocChon->setHeaderData(1,Qt::Horizontal,QObject::tr("Ten hang"));
            dsThuocChon->setHeaderData(2,Qt::Horizontal,QObject::tr("So luong"));
            dsThuocChon->setHeaderData(3,Qt::Horizontal,QObject::tr("Don gia"));
            dsThuocChon->setHeaderData(4,Qt::Horizontal,QObject::tr("Tong tien"));
        }
    }
}

QList<QStandardItem *> FormBanThuoc::add(const QString &maHang,
                                         const QString &tenHang,
                                         const QString &soLuong,
                                         const QString &donGia,
                                         const QString &tongTien)
{
    QList<QStandardItem *> rowItems;
    rowItems << new QStandardItem(maHang);
    rowItems << new QStandardItem(tenHang);
    rowItems << new QStandardItem(soLuong);
    rowItems << new QStandardItem(donGia);
    rowItems << new QStandardItem(tongTien);
    return rowItems;
}

void FormBanThuoc::lamMoiBangketQua()
{

    dsThuocChon->clear();
    ui->lineEditTongDonThuoc->setText("");
    queryModelKetQua->setQuery("SELECT 	HANGHOAID,TEN_HANG_HOA,SO_DU FROM HANG_HOA");
    queryModelKetQua->setHeaderData(0,Qt::Horizontal,"Mã hàng");
    queryModelKetQua->setHeaderData(1,Qt::Horizontal,"Tên hàng");
    queryModelKetQua->setHeaderData(2,Qt::Horizontal,"Số dư");
}

void FormBanThuoc::lamMoiNhomKH()
{
    cbModelNhom->setQuery("SELECT NHOM_KHACH_HANG.MO_TA_NHOM,NHOM_KHACH_HANG.NHOMID FROM NHOM_KHACH_HANG");
}


void FormBanThuoc::on_lineEditKhID_returnPressed()
{
    /*Truy van thong tin khach qua ma khach hang*/
    OKhachHang okh = khachHang->layThongTin(ui->lineEditKhID->text());
    ui->lineEditTenKh->setText(okh.TEN_KHACH_HANG);
    ui->textEditDiaChi->setText(okh.DIA_CHI_KH);
    ui->comboBoxNhomKh->setCurrentText(okh.NHOMID);
}

void FormBanThuoc::on_treeViewKetQua_clicked(const QModelIndex &index)
{
    ui->lineEditSodu->setText(queryModelKetQua->index(index.row(),2).data().toString());
    ui->lineEditDonGia->setText(gia->layGia(queryModelKetQua->index(index.row(),0).data().toString()));
    ui->lineEditMaHang->setText(queryModelKetQua->index(index.row(),0).data().toString());
    ui->lineEditTenHang->setText(queryModelKetQua->index(index.row(),1).data().toString());
    //truy van gia theo ma thuoc
}

void FormBanThuoc::on_pushButton_clicked()
{
    /*
     * Duyet danh sach thuoc luu tung dong vao
     * Tao hoa don HOADONID,USERNAME,KHACHHANGID,NGAY_LAP_HD
     * Nhap vao hoa don chi tiet HOADONID,THUOCID,SO_LUONG
    */
    QSharedPointer<TaiKhoan> taiKhoan(new TaiKhoan()); //lay nguoi dang nhap hien tai
    QSharedPointer<HoaDonRa> hoaDon(new HoaDonRa());
    QSharedPointer<OHoaDonRa> ohoaDon(new OHoaDonRa());
    QSharedPointer<ManageIndex> genIndex(new ManageIndex);
    ohoaDon.data()->TONG_SO_TIEN = ui->lineEditTongDonThuoc->text();
    ohoaDon.data()->USERNAME =taiKhoan.data()->layIDDangNhapHienTai();
   ohoaDon.data()->HOADONRAID = genIndex.data()->getNextIndexCode("HOA_DON_RA","DR");
    if(ui->lineEditKhID->text() != NULL){
        ohoaDon.data()->KHACHHANGID = ui->lineEditKhID->text();
        if(NULL != hoaDon.data()->taoMoi(*ohoaDon.data())){
            for (int i = 0; i < dsThuocChon->rowCount(); ++i) {
                QSharedPointer<OChiTietHoaDonRa> ochiTietHoaDonRa(new OChiTietHoaDonRa());
                ochiTietHoaDonRa.data()->HANGHOAID = dsThuocChon->index(i,0).data().toString();
                ochiTietHoaDonRa.data()->DON_GIA = dsThuocChon->index(i,3).data().toString();
                ochiTietHoaDonRa.data()->SO_LUONG = dsThuocChon->index(i,2).data().toString();
                ochiTietHoaDonRa.data()->TONG_SO_TIEN =dsThuocChon->index(i,4).data().toString();
                hoaDon.data()->chiTietHoaDonRa->append(*ochiTietHoaDonRa.data());
                //chitietHoaDon.data()->append(dsThuocChon->index(i,0).data().toString(),dsThuocChon->index(i,2).data().toString(), dsThuocChon->index(i,3).data().toString());
            }
            lamMoiBangketQua();
        }else{
            QMessageBox thongBao;
            thongBao.setIcon(QMessageBox::Warning);
            thongBao.setText("Khong the tiep tuc\nXin hay kiem tra lai thong tin nhap vao");
            thongBao.exec();
        }
    }else{
        //Kiem tra thong tin can thiet nhu nhom/ten/diachi
        if(ui->lineEditTenKh->text()==NULL || ui->comboBoxNhomKh->currentIndex() == -1){
           QMessageBox msg;
           msg.setText("Ban chua dien ten!");
           msg.exec();
        }else{
            /*Tao khach thong tin khach hang moi*/

            QSharedPointer<OKhachHang> okhachHang(new OKhachHang());
            okhachHang.data()->DIA_CHI_KH = ui->textEditDiaChi->document()->toPlainText();
            okhachHang.data()->NHOMID = cbModelNhom->index(ui->comboBoxNhomKh->currentIndex(),1).data().toString();
            okhachHang.data()->TEN_KHACH_HANG = ui->lineEditTenKh->text();
            QString makh = khachHang->themKhachHang(okhachHang.data());
            if(makh != NULL){
                //khoi tao hoa don lay duoc ma hoa don dua vao hoa don chi tiet
                ohoaDon.data()->KHACHHANGID = makh;
                hoaDon.data()->taoMoi(*ohoaDon.data());
                for (int i = 0; i < dsThuocChon->rowCount(); ++i) {
                    QSharedPointer<OChiTietHoaDonRa> ochiTietHoaDonRa(new OChiTietHoaDonRa());
                    ochiTietHoaDonRa.data()->HANGHOAID = dsThuocChon->index(i,0).data().toString();
                    ochiTietHoaDonRa.data()->DON_GIA = dsThuocChon->index(i,3).data().toString();
                    ochiTietHoaDonRa.data()->SO_LUONG = dsThuocChon->index(i,2).data().toString();
                    ochiTietHoaDonRa.data()->TONG_SO_TIEN =dsThuocChon->index(i,4).data().toString();
                    hoaDon.data()->chiTietHoaDonRa->append(*ochiTietHoaDonRa.data());
                }
                lamMoiBangketQua();
            }
        }
    }
}


void FormBanThuoc::on_lineEditSodu_textChanged(const QString &arg1)
{
    ui->spinBoxSoLuong->setMaximum(arg1.toInt(0));
}


void FormBanThuoc::tong(const QString &arg1)
{
    int tong = arg1.toInt(0) * ui->spinBoxSoLuong->text().toInt(0);
    ui->lineEditTongDonGia->setText(QString::number(tong));
}



void FormBanThuoc::on_spinBoxSoLuong_valueChanged(const QString &arg1)
{
     int tong = arg1.toInt(0) * ui->lineEditDonGia->text().toInt(0);
     ui->lineEditTongDonGia->setText(QString::number(tong));
}

void FormBanThuoc::on_lineEditDonGia_textChanged(const QString &arg1)
{
    tong(arg1);
}

void FormBanThuoc::on_treeViewKetQua_activated(const QModelIndex &index)
{

}
void FormBanThuoc::customMenuRequested(QPoint pos)
{
    QModelIndex index=ui->tableViewThuocChon->indexAt(pos);
    ui->tableViewThuocChon->selectRow(index.row());
    QMenu *menu =new QMenu(this);
    menu->addAction(new QAction("Delete", this));
    menu->popup(ui->tableViewThuocChon->viewport()->mapToGlobal(pos));
    connect(menu,SIGNAL(triggered(QAction*)),this,SLOT(xoaMotHang(QAction*)));
}

void FormBanThuoc::xoaMotHang(QAction *id)
{
    if(id->text() == "Delete"){
        dsThuocChon->removeRow(ui->tableViewThuocChon->currentIndex().row());
    }
}

void FormBanThuoc::on_checkBox_clicked(bool checked)
{
    if( checked){
        ui->spinBoxSoLuong->setMaximum(99);
    }else{
        ui->spinBoxSoLuong->setMaximum(ui->lineEditSodu->text().toInt(0));
    }
}

void FormBanThuoc::on_toolButtonThemNhom_clicked()
{
    FormNhomKhachHang *nhomkh = new FormNhomKhachHang();
    nhomkh->setAttribute(Qt::WA_DeleteOnClose,true);
    connect(nhomkh,SIGNAL(lamMoi()),this,SLOT(lamMoiNhomKH()));
    nhomkh->show();
}
