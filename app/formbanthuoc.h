#ifndef FORMBANTHUOC_H
#define FORMBANTHUOC_H

#include <QWidget>
#include <QSqlQueryModel>
#include <QStandardItemModel>
#include "utils/thuoc.h"
#include "Objects/okhachhang.h"
#include "utils/khachhang.h"
#include "utils/taikhoan.h"
#include "utils/chitiethoadon.h"
#include <QMessageBox>
#include "utils/gia.h"
#include <QMenu>
#include "utils/hoadonra.h"
#include "chucnang/formnhomkhachhang.h"
#include <QDebug>

namespace Ui {
class FormBanThuoc;
}

class FormBanThuoc : public QWidget
{
    Q_OBJECT

public:
    explicit FormBanThuoc(QWidget *parent = 0);
    ~FormBanThuoc();

private slots:
    void on_lineEditTim_textChanged(const QString &arg1);

    void on_pushButton_2_clicked();

    void on_lineEditKhID_returnPressed();

    void on_treeViewKetQua_clicked(const QModelIndex &index);

    void on_pushButton_clicked();

    void on_lineEditSodu_textChanged(const QString &arg1);
    void tong(const QString &arg1);
    void on_spinBoxSoLuong_valueChanged(const QString &arg1);
    void on_lineEditDonGia_textChanged(const QString &arg1);

    void on_treeViewKetQua_activated(const QModelIndex &index);

private:
    Ui::FormBanThuoc *ui;
    QSqlQueryModel *queryModelKetQua;
    QStandardItemModel *dsThuocChon;
    QList<QStandardItem *> add(const QString &maHang,
                               const QString &tenHang,
                               const QString &soLuong, const QString &donGia, const QString &tongTien);
    KhachHang *khachHang;
    QSqlQueryModel *cbModelNhom;
    Gia *gia;
    QSqlQueryModel *cbNhomHangModel;
    void lamMoiBangketQua();

private slots:
    void customMenuRequested(QPoint pos);
        void xoaMotHang(QAction *id);
        void on_checkBox_clicked(bool checked);
        void on_toolButtonThemNhom_clicked();
        void lamMoiNhomKH();
};

#endif // FORMBANTHUOC_H
