#ifndef XUATHANGMODEL_H
#define XUATHANGMODEL_H
#define LE "L"
#define PHEP "P"
#define NGHIBU "NB"
#define NGHICHEDO "CD"
#define OM "O"
#define CHEDOBH "BH"
#define VIECRIENG "VR"
#define KHONGPHEP "KP"
#define NGHITB "TB"
#define NGOAIGIONT "NGNT"
#define NGOAIGIOCN "NGCN"
#define NGOAIGIOLE "NGLE"
#define NGOAIGIO "NG"
#define TRUC "TR"
#define TRUCCN "TC"
#define TRUCLE "TL"
#define TONGCONG "TONG"
#define NGAY "NGAY"
#define CONGTAC "CT"
#define NGAYCONG "CONGCHINH"
#define CHINH "X"
#define STR_NGOAI_GIO "NGOÀI GIỜ"
#define STR_CHINH "CHÍNH"
#define STR_HE_SO "HỆ SỐ"
#define STR_NGAY "NGÀY"
#define STR_MA_HANG "Mã hàng"
#define STR_SO_LUONG "Số lượng"
#define STR_DON_GIA "Đơn giá"
#define STR_TONG_TIEN "Tổng"
#define STR_DON_VI_TINH "ĐVT"
#define STR_TEN_HANG "Tên hàng"

#include <QObject>
#include <QAbstractTableModel>
#include <QSqlQuery>
#include <QJsonDocument>
#include <QJsonObject>
#include <QColor>
#include <QBrush>
#include <QStandardItemModel>
#include <QDebug>
class XuatHangModel : public QAbstractTableModel
{
    Q_OBJECT
public:
    explicit XuatHangModel(QStandardItemModel *model, QObject *parent = 0);
    ~XuatHangModel();
    bool laMayChaml;
    QVariant data(const QModelIndex &index, int role) const Q_DECL_OVERRIDE;
    QVariant headerData(int section, Qt::Orientation orientation,
                        int role = Qt::DisplayRole) const Q_DECL_OVERRIDE;

    int rowCount(const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;
    int columnCount(const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;
    //cho phep sua du lieu
    Qt::ItemFlags flags(const QModelIndex &index) const;
     bool setData(const QModelIndex &index, const QVariant &value, int role);

signals:
     void thayDoiHeSo(int index);
private:
    QStandardItemModel *hangHoaXuatModel;
    void setKyHieu(int rowIndex, const QString value);
    //tinh mau cho tung dong
    QColor calculateColorForRow(int row) const;
public slots:
    bool insertRow(QModelIndex index, int dau, int last);
};

#endif // XUATHANGMODEL_H
