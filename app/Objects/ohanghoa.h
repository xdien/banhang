#ifndef OHANGHOA_H
#define OHANGHOA_H
#include <QString>

class OHangHoa
{
public:
    OHangHoa(const QString &HOANGHOAID, const QString &DVTID, const QString &NHOM_HANG_HOA_ID,
             const QString &TEN_HANG_HOA, const QString &DIEN_GIAI);
    QString HOANGHOAID, DVTID,NHOM_HANG_HOA_ID,TEN_HANG_HOA,DIEN_GIAI,SO_DU;
};

#endif // OHANGHOA_H
