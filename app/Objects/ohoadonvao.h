#ifndef OHOADONVAO_H
#define OHOADONVAO_H
#include <QString>
#include "okhachhang.h"
#include <QDate>

class OHoaDonVao
{
public:
    OHoaDonVao();
    QString HOADONVAOID,USERNAME,	MAU_SO,KY_HIEU,SO_HIEU;
    QDate NGAY_LAP_HDV;
    double tongTienHang,tongTienCuoi,vat,tienThue;
    OKhachHang okhachHang;
};

#endif // OHOADONVAO_H
