#include "sqlmanage.h"

SQLManage::SQLManage(QObject *parent) : QObject(parent)
{

}

SQLManage::~SQLManage()
{
    qDebug() << "xoa SQLManage";
}

QString SQLManage::getUserName()
{
    return userName;
}

QString SQLManage::getPass()
{
    return pass;
}

QString SQLManage::getHost()
{
    return host;
}

QString SQLManage::getSelectDb()
{
    return selectDb;
}

int SQLManage::getPort()
{
    return port;
}

void SQLManage::setUserName(QString username)
{
    this->userName = username;
}

void SQLManage::setPass(QString pass)
{
    this->pass = pass;
}

void SQLManage::setHost(QString host)
{
    this->host = host;
}

void SQLManage::setSelectDb(QString dbName)
{
    this->selectDb = dbName;
}

void SQLManage::setPort(int port)
{
    this->port = port;
}

