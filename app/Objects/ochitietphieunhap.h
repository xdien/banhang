#ifndef OCHITIETPHIEUNHAP_H
#define OCHITIETPHIEUNHAP_H

#include <QObject>


class OChiTietPhieuNhap : public QObject
{
    Q_OBJECT
public:
    explicit OChiTietPhieuNhap(QObject *parent = 0);
    ~OChiTietPhieuNhap();
    QString phieuNhapId, soLo, soLuong, donGia;
private:
signals:

public slots:
};

#endif // OCHITIETPHIEUNHAP_H
