#ifndef DDH_H
#define DDH_H

#include <QObject>
#include <QStandardItemModel>

class DDH : public QObject
{
    Q_OBJECT
public:
    explicit DDH(QObject *parent = 0);
    ~DDH();
    QString id, ngayLap;
    QStandardItemModel *noiDung;
signals:

public slots:
private:

};

#endif // DDH_H
