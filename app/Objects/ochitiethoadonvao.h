#ifndef OCHITIETHOADONVAO_H
#define OCHITIETHOADONVAO_H
#include <QString>

class OChiTietHoaDonvao
{
public:
    OChiTietHoaDonvao(const QString HOANGHOAID, const QString SO_LUONG, const QString DON_GIA, const QString TONG_SO_TIEN);
    QString HOANGHOAID,SO_LUONG,DON_GIA,TONG_SO_TIEN,SO_DU_TRUOC;

private:
    QString HOADONVAOID;
};

#endif // OCHITIETHOADONVAO_H
