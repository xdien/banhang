#include "xuathangmodel.h"

XuatHangModel::XuatHangModel(QStandardItemModel *model, QObject *parent) :
    QAbstractTableModel(parent)
{
   hangHoaXuatModel = model;

   //connect(hangHoaXuatModelSIGNAL(rowsInserted(QModelIndex,int,int)),this,SLOT(insertRow(QModelIndex,int,int)));
}

XuatHangModel::~XuatHangModel()
{
    qDebug() << "xoa XuatHangModel";
}

QVariant XuatHangModel::data(const QModelIndex &index, int role) const
{
    int vitri = index.row();
    switch (role) {//xac dinh quy dinh dang yeu cau
    case Qt::DisplayRole:
    {
        switch (index.column()) {
        case 0: //ngay N
            return hangHoaXuatModel->index(index.row(),0);
            break;
        case 1: //Cong CONG
            return hangHoaXuatModel->index(index.row(),1);
                break;
        case 2://NGOAI GIO NG
            return hangHoaXuatModel->index(index.row(),2);
            break;
        case 3://NGOAI GIO TONG
            return hangHoaXuatModel->index(index.row(),3);
            break;
        case 4://NGOAI GIO TONG
            return hangHoaXuatModel->index(index.row(),4);
            break;
        case 5://NGOAI GIO TONG
            return hangHoaXuatModel->index(index.row(),5);
            break;
        default:
            return QVariant();
            break;
        }
    }
        break;
    case Qt::BackgroundRole:
    {
        QColor color = calculateColorForRow(vitri);
        return QBrush(color);
    }
        break;
    default:
        return QVariant::Invalid;
        break;
    }

}

QVariant XuatHangModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal && role == Qt::DisplayRole)
    {
        switch (section) {
        case 0:
            return STR_MA_HANG;
            break;
        case 1:
            return STR_TEN_HANG;
            break;
        case 2:
            return STR_DON_VI_TINH;
            break;
        case 3:
            return STR_SO_LUONG;
            break;
        case 4:
            return STR_DON_GIA;
            break;
        case 5:
            return STR_TONG_TIEN;
            break;
        default:
            break;
        }
    }

    return QVariant();
}

int XuatHangModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return hangHoaXuatModel->rowCount();
}

int XuatHangModel::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return 6;
}

Qt::ItemFlags XuatHangModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return Qt::ItemIsEnabled;

    return QAbstractTableModel::flags(index) | Qt::ItemIsEditable;
}

bool XuatHangModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    qDebug() << value;
    if (index.isValid() && role == Qt::EditRole) {
        int vitri = index.row();
        switch (index.column()) {
        case 0:
            hangHoaXuatModel->index(vitri,0);
            break;
        case 1:
            hangHoaXuatModel->index(vitri,1);
            break;
        case 2:
            hangHoaXuatModel->index(vitri,2);
            break;
        case 3:
            hangHoaXuatModel->index(vitri,3);
            break;
        case 4:
            hangHoaXuatModel->index(vitri,4);
            break;
        case 5:
            hangHoaXuatModel->index(vitri,5);
            break;
        default:
            break;
        }

        return true;
    }

    return false;
}

QColor XuatHangModel::calculateColorForRow(int row) const
{
    /*if(chamCong->danhSachNgay.at(row)->value(LE).toInt() == 2) //ngay tb hoi do
        return QColor::fromRgb(204, 153, 255);
    if(chamCong->danhSachNgay.at(row)->value(LE).toInt() == 1)// ngay le hoi xanh
        return QColor::fromRgb(102, 153, 255);
    if(chamCong->danhSachNgay.at(row)->value(THU).toInt(0) == 7)//ngay chu nhat
        return QColor::fromRgb(240, 168, 204);*/
    return Qt::white;
}

bool XuatHangModel::insertRow(QModelIndex index, int dau, int last)
{

}



