#ifndef ONHOMHANGHOA_H
#define ONHOMHANGHOA_H
#include <QString>

class ONhomHangHoa
{
public:
    ONhomHangHoa(const QString &NHOM_HANG_HOA_ID, const QString &NHO_NHOM_HANG_HOA_ID, const QString &MO_TA);
    QString NHOM_HANG_HOA_ID, NHO_NHOM_HANG_HOA_ID, MO_TA;
};

#endif // ONHOMHANGHOA_H
