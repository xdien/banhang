#ifndef OKHACHHANG_H
#define OKHACHHANG_H

#include <QObject>

class OKhachHang
{
public:
    explicit OKhachHang();
    QString KHACHHANGID, NHOMID, TEN_KHACH_HANG,DIA_CHI_KH;
    QString MST,tenDonVi,mauSoHD,KyHieuHD,sdt;
    OKhachHang &operator=(const OKhachHang &okh);//phep toan = cho doi tuong trong c++
};

#endif // OKHACHHANG_H
