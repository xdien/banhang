#ifndef SQLMANAGE_H
#define SQLMANAGE_H
#include <QString>
#include <QtSql>

class SQLManage : public QObject
{
    Q_OBJECT
public:
    explicit SQLManage(QObject *parent = 0);
    ~SQLManage();
    QString getUserName();
    QString getPass();
    QString getHost();
    QString getSelectDb();
    int getPort();
    void setUserName(QString username);
    void setPass(QString pass);
    void setHost(QString host);
    void setSelectDb(QString dbName);
    void setPort(int port);
private:
    QString userName,pass,host,selectDb;
    int port;

signals:

public slots:
};

#endif // SQLMANAGE_H
