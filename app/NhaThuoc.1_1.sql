/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     5/5/2016 1:28:23 PM                          */
/*==============================================================*/


drop table if exists BANG_GIA;

drop table if exists CHI_TIET_HOA_DON;

drop table if exists CHI_TIET_NHAP;

drop table if exists DON_DAT_HANG;

drop table if exists HOA_DON;

drop table if exists KHACH_HANG;

drop table if exists NHA_CUNG_CAP;

drop table if exists NHA_SAN_XUAT;

drop table if exists NHOM_KHACH_HANG;

drop table if exists PHIEU_CHI;

drop table if exists PHIEU_NHAP;

drop table if exists QUYEN;

drop table if exists SO_LO;

drop table if exists TAI_KHOAN;

drop table if exists THUOC;

/*==============================================================*/
/* Table: BANG_GIA                                              */
/*==============================================================*/
create table BANG_GIA
(
   GIAID                varchar(15) not null,
   THUOCID              varchar(12) not null,
   GIA_THUOC            float,
   NGAY_AP_DUNG         date,
   primary key (GIAID)
);

alter table BANG_GIA comment 'Gia thu?c l?y ng�y cu?i c�ng l�m gi� hi?n t?i
';

/*==============================================================*/
/* Table: CHI_TIET_HOA_DON                                      */
/*==============================================================*/
create table CHI_TIET_HOA_DON
(
   HOADONID             varchar(15) not null,
   THUOCID              varchar(12) not null,
   SO_LUONG             int,
   DON_GIA              bigint,
   primary key (HOADONID, THUOCID)
);

/*==============================================================*/
/* Table: CHI_TIET_NHAP                                         */
/*==============================================================*/
create table CHI_TIET_NHAP
(
   PHIEUNHAPID          char(12) not null,
   SO_LO                varchar(20) not null,
   SO_LUONG_NHAP        int,
   DON_GIA_NHAP         bigint,
   primary key (PHIEUNHAPID, SO_LO)
);

/*==============================================================*/
/* Table: DON_DAT_HANG                                          */
/*==============================================================*/
create table DON_DAT_HANG
(
   DONDATHANGID         char(12) not null,
   NGAY_LAP_DDH         timestamp,
   NO_DDH               text,
   primary key (DONDATHANGID)
);

/*==============================================================*/
/* Table: HOA_DON                                               */
/*==============================================================*/
create table HOA_DON
(
   HOADONID             varchar(15) not null,
   USERNAME             varchar(32) not null,
   KHACHHANGID          varchar(15) not null,
   NGAY_LAP_HD          timestamp default CURRENT_TIMESTAMP,
   TONG_SO_TIEN         bigint,
   primary key (HOADONID)
);

/*==============================================================*/
/* Table: KHACH_HANG                                            */
/*==============================================================*/
create table KHACH_HANG
(
   KHACHHANGID          varchar(15) not null,
   NHOMID               varchar(15) not null,
   TEN_KHACH_HANG       varchar(33),
   DIA_CHI_KH           text,
   primary key (KHACHHANGID)
);

/*==============================================================*/
/* Table: NHA_CUNG_CAP                                          */
/*==============================================================*/
create table NHA_CUNG_CAP
(
   NHACUNGCAPID         char(12) not null,
   TEN_NCC              text,
   DIA_CHI_NCC          text,
   primary key (NHACUNGCAPID)
);

/*==============================================================*/
/* Table: NHA_SAN_XUAT                                          */
/*==============================================================*/
create table NHA_SAN_XUAT
(
   NHASANXUATID         char(12) not null,
   TEN_NSX              text,
   QUOC_GIA             varchar(30),
   _IA_CHI_NSX          text,
   primary key (NHASANXUATID)
);

alter table NHA_SAN_XUAT comment 'Ch? c� 1 nh� s?n xu?t ra m?t lo?i thu?c, n?u thu?c d� c� c�n';

/*==============================================================*/
/* Table: NHOM_KHACH_HANG                                       */
/*==============================================================*/
create table NHOM_KHACH_HANG
(
   NHOMID               varchar(15) not null,
   MO_TA_NHOM           text,
   primary key (NHOMID)
);

/*==============================================================*/
/* Table: PHIEU_CHI                                             */
/*==============================================================*/
create table PHIEU_CHI
(
   PHIEUCHIID           varchar(15) not null,
   USERNAME             varchar(32) not null,
   NGAY_LAP_PC          timestamp,
   SO_TIEN              bigint,
   GHI_CHU              text,
   primary key (PHIEUCHIID)
);

/*==============================================================*/
/* Table: PHIEU_NHAP                                            */
/*==============================================================*/
create table PHIEU_NHAP
(
   PHIEUNHAPID          char(12) not null,
   DONDATHANGID         char(12) not null,
   USERNAME             varchar(32) not null,
   NGAY_LAP_PN          timestamp,
   primary key (PHIEUNHAPID)
);

alter table PHIEU_NHAP comment 'H?i khi nh?p h�ng v?(mua t?i ch? du?c) th� mua l? m?t l�c c�';

/*==============================================================*/
/* Table: QUYEN                                                 */
/*==============================================================*/
create table QUYEN
(
   FORMID               varchar(45) not null,
   MO_TA                varchar(50),
   XEM                  bool,
   CAP_NHAT             bool,
   primary key (FORMID)
);

/*==============================================================*/
/* Table: SO_LO                                                 */
/*==============================================================*/
create table SO_LO
(
   SO_LO                varchar(20) not null,
   NHASANXUATID         char(12) not null,
   NHACUNGCAPID         char(12) not null,
   NGAY_SAN_XUAT        date,
   NGAY_HET_HAN         date,
   primary key (SO_LO)
);

alter table SO_LO comment '1 l� c?a c�ng ty s?n xu?t th� l� 1 m?t h�ng duy nh?t
H';

/*==============================================================*/
/* Table: TAI_KHOAN                                             */
/*==============================================================*/
create table TAI_KHOAN
(
   USERNAME             varchar(32) not null,
   PASSWORD             varchar(30),
   TEN_NV               varchar(32),
   GIOI_TINH            bool,
   DIA_CHI_NV           text,
   COQUYEN              text,
   primary key (USERNAME)
);

/*==============================================================*/
/* Table: THUOC                                                 */
/*==============================================================*/
create table THUOC
(
   THUOCID              varchar(12) not null,
   SO_LO                varchar(20) not null,
   TEN_THUOC            varchar(35),
   QUY_CACH__ONG_GOI    varchar(30),
   DIEN_GIAI            text,
   SO_DU                int,
   primary key (THUOCID)
);

alter table BANG_GIA add constraint FK_AP_DUNG_GIA foreign key (THUOCID)
      references THUOC (THUOCID) on delete restrict on update restrict;

alter table CHI_TIET_HOA_DON add constraint FK_CHI_TIET_THUOC foreign key (THUOCID)
      references THUOC (THUOCID) on delete restrict on update restrict;

alter table CHI_TIET_HOA_DON add constraint FK_HOA_DON_CHI_TIET foreign key (HOADONID)
      references HOA_DON (HOADONID) on delete restrict on update restrict;

alter table CHI_TIET_NHAP add constraint FK_CHI_TIET_LO_NHAP foreign key (SO_LO)
      references SO_LO (SO_LO) on delete restrict on update restrict;

alter table CHI_TIET_NHAP add constraint FK_PHIEU_NHAP_CHI_TIET foreign key (PHIEUNHAPID)
      references PHIEU_NHAP (PHIEUNHAPID) on delete restrict on update restrict;

alter table HOA_DON add constraint FK_LAP_HOA_DON foreign key (USERNAME)
      references TAI_KHOAN (USERNAME) on delete restrict on update restrict;

alter table HOA_DON add constraint FK_MUA_THUOC foreign key (KHACHHANGID)
      references KHACH_HANG (KHACHHANGID) on delete restrict on update restrict;

alter table KHACH_HANG add constraint FK_THUOC_NHOM_KHACH foreign key (NHOMID)
      references NHOM_KHACH_HANG (NHOMID) on delete restrict on update restrict;

alter table PHIEU_CHI add constraint FK_LAP_PHIEU_CHI foreign key (USERNAME)
      references TAI_KHOAN (USERNAME) on delete restrict on update restrict;

alter table PHIEU_NHAP add constraint FK_CO_PHIEU foreign key (DONDATHANGID)
      references DON_DAT_HANG (DONDATHANGID) on delete restrict on update restrict;

alter table PHIEU_NHAP add constraint FK_DOI_CHIEU foreign key (USERNAME)
      references TAI_KHOAN (USERNAME) on delete restrict on update restrict;

alter table SO_LO add constraint FK_THUOC_NHA_CC foreign key (NHACUNGCAPID)
      references NHA_CUNG_CAP (NHACUNGCAPID) on delete restrict on update restrict;

alter table SO_LO add constraint FK_THUOC_NHA_SX foreign key (NHASANXUATID)
      references NHA_SAN_XUAT (NHASANXUATID) on delete restrict on update restrict;

alter table THUOC add constraint FK_THUOC_LO foreign key (SO_LO)
      references SO_LO (SO_LO) on delete restrict on update restrict;

