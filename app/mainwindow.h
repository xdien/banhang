#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QSqlQuery>
#include <QMdiSubWindow>
#include <QLabel>
#include "login.h"
#include <QProgressBar>
#include <QtSql>
#include <QMessageBox>
#include <QObjectList>
#include "about.h"
#include "formbanthuoc.h"
#include "chucnang/bctonkho.h"
#include "chucnang/formnhomkhachhang.h"
#include "lrreportengine.h" //to add report engine
#include "lrcallbackdatasourceintf.h" //if you want use callback datasources
#include "chucnang/formdathang.h"
#include "chucnang/formquyen.h"
#include "chucnang/formtaikhoan.h"
#include "chucnang/formphieuchi.h"
#include "chucnang/formphieunhap.h"
#include "chucnang/dialogkhachhang.h"
#include "chucnang/formgia.h"
#include "utils/quyen.h"
#include "chucnang/formbaocaohoadon.h"
#include "chucnang/formdauvao.h"
#include "chucnang/formnhomhanghoa.h"
#include "chucnang/formhanghoa.h"
#include "chucnang/bcdaura.h"
#include "chucnang/xuathoadonform.h"
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
private:
    bool timQuyen(QString formid);
signals:

private slots:
    void on_actionSQL_triggered();

    void on_actionAbout_Qt_triggered();
    void capnhatPhanQuyen(QString username);

   void on_actionAbout_triggered();
   void on_actionDan_nhap_triggered();
     void on_actionBan_triggered();

     void on_actionQuan_ly_NKH_triggered();

     void on_actionQuyen_triggered();

     void on_actionTai_Khoan_triggered();

     void on_actionPhieu_chi_triggered();

     void on_actionKhach_Hang_triggered();

     void on_actionGia_triggered();

     void on_actionH_a_n_triggered();

     void on_actionNhapHang_2_triggered();

     void on_actionNhapHang_triggered();

     void on_actionNhom_Hang_Hoa_triggered();

     void on_actionHang_Hoa_triggered();

     void on_actionTon_kho_triggered();

     void on_actionSua_mau_hd_triggered();

private:
    Ui::MainWindow *ui;
    QSqlQuery query;
    QSqlDatabase db;
    login *moi;

    QList<QMdiSubWindow*> subwindowList;
    QObjectList objl;
    QLabel *statusLabel, *tinhtrangsql;
    QSqlQueryModel quyenTk;
    QJsonArray ar;
    QProgressBar *tienTrinh;
};

#endif // MAINWINDOW_H
