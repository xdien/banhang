#ifndef KHACHHANG_H
#define KHACHHANG_H

#include <QObject>
#include <Objects/okhachhang.h>

class KhachHang : public QObject
{
    Q_OBJECT
public:
    explicit KhachHang(QObject *parent = 0);
    QString themKhachHang(OKhachHang *oKhachhang);
    const OKhachHang layThongTin(const QString khachHangID);
    bool xoaKhachHang(QString khid);
signals:

public slots:
};

#endif // KHACHHANG_H
