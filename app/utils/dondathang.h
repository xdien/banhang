#ifndef DONDATHANG_H
#define DONDATHANG_H

#include <QObject>
#include <QSqlQuery>
#include "Objects/ddh.h"
#include "utils/manageindex.h"
#include <QJsonObject>
#include <QStandardItemModel>


class DonDatHang : public QObject
{
    Q_OBJECT
public:
    explicit DonDatHang(QObject *parent = 0);
    ~DonDatHang();
    bool themDDH(DDH *donDatHang);
    QString standarModelToStrJson(const QStandardItemModel *model);
    QString truyVanBangGoiY, truyVanNCC,truyVanNSX;
private:

signals:

public slots:
};

#endif // DONDATHANG_H
