#ifndef QUYEN_H
#define QUYEN_H

#include <QObject>
#include <QSqlTableModel>
#include "Objects/oquyen.h"
#include "utils/manageindex.h"

class Quyen : public QObject
{
    Q_OBJECT
public:
    explicit Quyen(QObject *parent = 0);
    ~Quyen();
    bool themQuyen(const OQuyen *quyen);
    QSqlQueryModel *layQuyen(QString username);
private:
    QSqlTableModel *tableModelQuyen;
    QSqlQuery *query;
signals:

public slots:
};

#endif // QUYEN_H
