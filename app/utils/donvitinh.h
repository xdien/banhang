#ifndef DONVITINH_H
#define DONVITINH_H

#include <QObject>
#include <QSqlQuery>
struct ODonViTinh{
    QString donViTinh;
};

class DonViTinh : public QObject
{
    Q_OBJECT
public:
    explicit DonViTinh(QObject *parent = 0);
    bool themDonVi(const QString &donvitinh);
    bool kiemTraDonVi(const QString &donViId);
signals:
private:
    QSqlQuery query;

public slots:
};

#endif // DONVITINH_H
