#ifndef PHIEUCHI_H
#define PHIEUCHI_H

#include <QObject>
#include "Objects/ophieuchi.h"
#include <QSqlTableModel>
#include "utils/manageindex.h"

class PhieuChi : public QObject
{
    Q_OBJECT
public:
    explicit PhieuChi(QObject *parent = 0);
    ~PhieuChi();
    bool themPhieuChi(OPhieuChi *oPhieuchi);
private:

signals:

public slots:
};

#endif // PHIEUCHI_H
