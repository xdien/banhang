#ifndef NHACUNGCAP_H
#define NHACUNGCAP_H

#include <QObject>
#include "Objects/ncc.h"
#include "utils/manageindex.h"

class NhaCungCap : public QObject
{
    Q_OBJECT
public:
    explicit NhaCungCap(QObject *parent = 0);
    ~NhaCungCap();
    bool themNCC(NCC *nhacungcap);
signals:

public slots:
};

#endif // NHACUNGCAP_H
