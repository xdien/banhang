#ifndef QLNHASANXUAT_H
#define QLNHASANXUAT_H
#include "Objects/nsx.h"

#include <QCompleter>
#include <QObject>
#include <QSqlQuery>
#include <QSqlTableModel>
#include <QVariant>
#include <QDebug>
#include <utils/manageindex.h>

class QLNhaSanXuat : public QObject
{
    Q_OBJECT
public:
    explicit QLNhaSanXuat(QObject *parent = 0);
    ~QLNhaSanXuat();
    bool themNSX(NSX *nhasx);
    void xoaNSX(QString id);
    void capNhat(NSX nhasx);
    QCompleter *layDanhSachQuocGia();
signals:

public slots:
};

#endif // QLNHASANXUAT_H
