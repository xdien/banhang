#include "chitietphieunhap.h"

#include <QJsonObject>
#include <QDebug>

ChiTietPhieuNhap::ChiTietPhieuNhap(QString phieuNhapID, QObject *parent) : QObject(parent)
{
    this->phieuNhapID = phieuNhapID;
}

ChiTietPhieuNhap::~ChiTietPhieuNhap()
{

}

QStandardItemModel *ChiTietPhieuNhap::layDuDatDangJSON(const QString donDatHangId)
{
    QSqlQuery query;
    QStandardItemModel *model;
    query.exec("select no_ddh from DON_DAT_HANG where DONDATHANGID = '"+donDatHangId+"'");
    if(query.next()){
        QJsonDocument doc = QJsonDocument::fromJson(query.value(0).toString().toUtf8());
        if(doc.isNull()){
            return NULL;
        }else{
            if(doc.isObject()){
                qDebug() << "Khong dung kieu du lieu: "<< query.value(0).toString();
                return NULL;
            }else{
                if(doc.isArray()){
                    model = new QStandardItemModel();
                    QJsonArray ar;
                ar = doc.array();
                for (int i = 0; i < ar.count(); ++i) {
                    if(ar[i].isObject()){
                        model->appendRow(add(ar[i].toObject().value("ten_thuoc").toString(""),
                                             ar[i].toObject().value("ten_ncc").toString(""),
                                             ar[i].toObject().value("ten_nsx").toString(""),
                                             ar[i].toObject().value("ma_ncc").toString(""),
                                             ar[i].toObject().value("ma_nsx").toString(""),
                                             ar[i].toObject().value("so_luong").toString("")));
                    }
                }
                return model;
                }else{

                    qDebug() << "Khong biet loi gi dang xay ra!!!";
                    return NULL;
                }
            }
        }
    }else{
        qDebug() << query.lastError().text();
        return NULL;
    }
}

QList<QStandardItem *> ChiTietPhieuNhap::add(const QString &tenThuoc, const QString &tenNCC, const QString &tenNSX, const QString &idNCC, const QString &idNSX, const QString &soLuong)
{
    QList<QStandardItem *> rowItems;
    rowItems << new QStandardItem(tenThuoc);
    rowItems << new QStandardItem(tenNCC);
    rowItems << new QStandardItem(tenNSX);
    rowItems << new QStandardItem(idNCC);
    rowItems << new QStandardItem(idNSX);
    rowItems << new QStandardItem(soLuong);
    return rowItems;
}

