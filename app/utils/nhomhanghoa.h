#ifndef NHOMHANGHOA_H
#define NHOMHANGHOA_H

#include <QObject>
#include "Objects/onhomhanghoa.h"
#include <QSqlQuery>
#include <QSqlError>
#include <QDebug>

class NhomHangHoa : public QObject
{
    Q_OBJECT
public:
    explicit NhomHangHoa(QObject *parent = 0);
    bool themNhom(const ONhomHangHoa &onhomHangHoa);
signals:
private:
    QSqlQuery query;

public slots:
};

#endif // NHOMHANGHOA_H
