#include "xuathoadon.h"

XuatHoaDon::XuatHoaDon(QObject *parent) : QObject(parent)
{


}

bool XuatHoaDon::themMoi(QStandardItemModel *danhSachChiTietHang)
{
    query.clear();
    query.prepare("INSERT INTO `HOA_DON_RA` (`HOADONRAID`, "
                  "`KHACHHANGID`, `NGAY_XUAT`, `MAU_SO`, `KY_HIEU`, "
                  "`SO_HIEU`, `MST`, `TEN_KHACH_HANG`, `TEN_DON_VI`, "
                  "`KIEU_THANH_TOAN`, `DIA_CHI`, `TONG_TIEN_HANG`, "
                  "`THUE_SUAT`, TIEN_THUE, `TONG_SO_TIEN`, `SO_TIEN_BANG_CHU`, "
                  "`BI_XOA`, `DA_IN`) VALUES "
                  "(:hdid, :khid, :ngayXuat, :mauSo, :kyHieu, :soHieu, :mst, :tenKhachHang, :tenDonVi, :kieuThanhToan,"
                  ":diaChi, :tongTienHang, :thueSuat,:tienThue, :tongSoTien, :soTienBangChu, false, true);");
    query.bindValue(":hdid",hdid);
    query.bindValue(":khid",khid);
    query.bindValue(":ngayXuat",ngayXuat);
    query.bindValue(":mauSo",mauSo);
    query.bindValue(":kyHieu",kyHieu);
    query.bindValue(":soHieu",soHieu);
    query.bindValue(":mst",mst);
    query.bindValue(":tenKhachHang",tenKhachHang);
    query.bindValue(":tenDonVi",tenDonVi);
    query.bindValue(":kieuThanhToan",kieuThanhToan);
    query.bindValue(":diaChi",diaChi);
    query.bindValue(":tienThue",tienThue);
    query.bindValue(":tongTienHang",tongTienHang);
    query.bindValue(":thueSuat",thueSuat);
    query.bindValue(":tongSoTien",tongSoTien);
    query.bindValue(":soTienBangChu",soTienBangChu);
    if(query.exec()){
        //chay chi tiet
        query.clear();
        if(query.exec(taoTruyVanThemDanhSach(danhSachChiTietHang,hdid)))
            if(query.numRowsAffected() != -1){
                qDebug() << "thanh cong";
                return true;
            }
            else
                return false;
        else
            return false;

    }else{
        qDebug() << "khong the them moi "<< query.lastError().text();
        return false;
    }
}

bool XuatHoaDon::kiemTraLogic(int soHieu, QString ngayXuat)
{

}

void XuatHoaDon::inHoaDon(QString hdid)
{
    QScopedPointer<LimeReport::ReportEngine> report;
    report.reset(new LimeReport::ReportEngine());
    report->loadFromFile(QApplication::applicationDirPath()+"/reports/hoa_don_ra.lrxml");
    report->dataManager()->setReportVariable("hdid",hdid);
    report->previewReport();
}

QString XuatHoaDon::taoTruyVanThemDanhSach(const QStandardItemModel *danhSachChiTietHang, const QString hoaDonRaId)
{
    QString longString;
    longString.append("");
    //can tao triger so su truoc
    for (int i = 0; i < danhSachChiTietHang->rowCount(); ++i) {
        longString.append("INSERT INTO `CHI_TIET_RA` (`HOADONRAID`, "
                                  "`HANGHOAID`, `TEN_HANG`, `SO_LUONG`, `DON_GIA`, `TONG_SO_TIEN`, "
                                  "`DON_VI`) VALUES "
                                  "('"+hoaDonRaId+"', '"+danhSachChiTietHang->index(i,0).data().toString()+"',"
                                  " '"+danhSachChiTietHang->index(i,1).data().toString()+"',"
                                  " '"+danhSachChiTietHang->index(i,3).data().toString()+"', "
                                  "'"+danhSachChiTietHang->index(i,4).data().toString()+"', "
                                  "'"+danhSachChiTietHang->index(i,5).data().toString()+"', "
                                  "'"+danhSachChiTietHang->index(i,2).data().toString()+"');");
    }
    qDebug() << longString;
    return longString;
}

