#include "nhomkhachhang.h"

NhomKhachHang::NhomKhachHang(QObject *parent) : QObject(parent)
{

}

NhomKhachHang::~NhomKhachHang()
{

}

bool NhomKhachHang::themNKH(NKH *nkh)
{
    QSharedPointer<ManageIndex> taoIndex(new ManageIndex());
    QString indexStr;
    indexStr = taoIndex.data()->getNextIndexCode("NHOM_KHACH_HANG","NK");
    QSqlQuery query;
    QString que = "INSERT INTO NHOM_KHACH_HANG (NHOMID, MO_TA_NHOM,CHIET_KHAU) "
                  "VALUES ('"+indexStr+"', '"+nkh->moTa+"','"+nkh->chietKhau+"')";
    if(query.exec(que)){
        return true;
    }else{
        qDebug() << query.lastError().text();
        return false;
    }
}

