#ifndef CHITIETHOADONVAO_H
#define CHITIETHOADONVAO_H

#include <QObject>
#include "Objects/ochitiethoadonvao.h"
#include <QSqlQuery>
#include <QSqlError>
#include <QDebug>

class ChiTietHoaDonVao
{
public:
    ChiTietHoaDonVao(const QString hoaDonVaoID);
    bool append(const OChiTietHoaDonvao &oChiTietHoaDonvao);
private:
    QString hoaDonVaoID;
    QSqlQuery query;
signals:

public slots:
};

#endif // CHITIETHOADONVAO_H
