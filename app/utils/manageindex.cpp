#include "manageindex.h"

ManageIndex::ManageIndex()
{
    num_prefix = "00000000";
}

ManageIndex::~ManageIndex()
{
}

QString ManageIndex::getNextIndexCode(QString tableName, QString prefix)
{
    num_prefix = "00000";
    query.exec("select * from "+tableName+" order by 1 desc limit 1");
    if(query.next())
    {
        //lay ma ra, tach so , tang so len 1 va gop so do voi prefix
        index_code = query.value(0).toString();
        idx = index_code.mid(prefix.length()).toInt(0);
        idx = idx +1;
        str_num = QString::number(idx);//doi thanh string lay so ky tu
        index_code = prefix +num_prefix.mid(str_num.length())+str_num;
    }else{
        str_num = "0";
        qDebug() << "Can't get value form table name, default value";
        index_code = prefix +num_prefix.mid(str_num.length())+str_num;
    }
    return index_code;
}
