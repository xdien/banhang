#ifndef NHAPHANG_H
#define NHAPHANG_H

#include <QObject>
#include "utils/manageindex.h"
#include "Objects/ophieunhap.h"
#include "utils/nhaphang.h"
#include "utils/taikhoan.h"
class NhapHang : public QObject
{
    Q_OBJECT
public:
    explicit NhapHang(QObject *parent = 0);
    ~NhapHang();
    QString themDonNhapHang(OPhieuNhap *ophieuNhap);
private:
    TaiKhoan *taiKhoan;
signals:

public slots:
};

#endif // NHAPHANG_H
