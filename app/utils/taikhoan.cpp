#include "taikhoan.h"

TaiKhoan::TaiKhoan(QObject *parent) : QObject(parent)
{

}

TaiKhoan::~TaiKhoan()
{

}

bool TaiKhoan::themTaiKhoan(OTaiKhoan *otaiKhoan)
{
    QSqlQuery query;
    QString que = "INSERT INTO TAI_KHOAN (USERNAME, PASSWORD, TEN_NV, GIOI_TINH,DIA_CHI_NV,COQUYEN) "
                  "VALUES ('"+otaiKhoan->username+"', '"+ otaiKhoan->pass+"', '"+otaiKhoan->tenNV+"',"
                  +otaiKhoan->getGioiTinh()+",'"+otaiKhoan->diaChi+"','"+standarModelToStrJson(otaiKhoan->coQuyen)+"' )";
    if(query.exec(que)){
        return true;
    }else{
        qDebug() << query.lastError().text();
        return false;
    }
}

QString TaiKhoan::layIDDangNhapHienTai()
{
    QSqlDatabase db = QSqlDatabase::database("sqlite",true);
    db.setDatabaseName("app.log");
    if(db.open()){
        QSqlQuery query(db);
        query.exec("select username,thoi_gian from dang_nhap ORDER BY thoi_gian desc");
        if(query.next()){
            qDebug() << "Thoi gian dang nhap" << query.value(1).toString();
            db.close();
            return query.value(0).toString();
        }else{
            db.close();
            return "chua dang nhap";
        }
    }else{
        qDebug() << "Chu mo ket noi sqlite!";
        return "false";
    }
}

bool TaiKhoan::danNhap(const QString userName, const QString pass)
{
    /*Mo ket noi sqlite*/
    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE","sqlite");
    db.setDatabaseName("app.log");
    if(db.open()){
        QSqlQuery queryl(db);
        queryl.exec("create table if not exists  dang_nhap (id INTEGER PRIMARY KEY AUTOINCREMENT,username varchar(30) not null, "
                       "thoi_gian TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL)");
        QSqlQuery query;
        query.exec("select username,TEN_NV from TAI_KHOAN where password = '"+pass+"' and  username = '"+userName+"'");
        if(query.next()){
            queryl.exec("insert into dang_nhap(username) values('"+userName+"')");
            emit thongBaoDangNhap("Dang nhap thanh cong", query.value(1).toString());
            qDebug() << "Login succeed";
            db.close();
            return true;
        }else{
            qDebug() << "Login failed";
            db.removeDatabase("sqlite");
            db.close();
            return false;
        }
    }else{
        qDebug() << "KHong the ket noi toi sqlite: "<<db.lastError().text();
        return false;
    }

}

QString TaiKhoan::standarModelToStrJson(const QStandardItemModel *model)
{
    int soDong;
    QJsonArray ar;
    soDong = model->rowCount();
    for(int i = 0;i < soDong; i++){
        QJsonObject dongDuLieuJSON;
        dongDuLieuJSON["ten_thuoc"] = model->index(i,0).data().toString();
        dongDuLieuJSON["ten_ncc"] = model->index(i,1).data().toString();
        dongDuLieuJSON["ten_nsx"] = model->index(i,2).data().toString();
        dongDuLieuJSON["ma_ncc"] = model->index(i,3).data().toString();
        dongDuLieuJSON["ma_nsx"] = model->index(i,4).data().toString();
        ar.append(dongDuLieuJSON);
    }
    QJsonDocument doc(ar);
    QString strJson(doc.toJson(QJsonDocument::Compact));
    return strJson;
}
