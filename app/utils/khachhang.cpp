#include "khachhang.h"
#include "manageindex.h"

KhachHang::KhachHang(QObject *parent) : QObject(parent)
{

}

QString KhachHang::themKhachHang(OKhachHang *oKhachhang)
{
    QSqlQuery query;
    query.prepare("INSERT INTO KHACH_HANG (KHACHHANGID, NHOMID, TEN_KH, DIA_CHI_KH, TEN_DN, MST) "
                  "VALUES (:khachHangId, :nhomId, :tenKH,:diaChi,:tenDonVi,:mst)");
    query.bindValue(":khachHangId",oKhachhang->KHACHHANGID);
    query.bindValue(":nhomId",oKhachhang->NHOMID);
    query.bindValue(":tenKH",oKhachhang->TEN_KHACH_HANG);
    query.bindValue(":diaChi",oKhachhang->DIA_CHI_KH);
    query.bindValue(":tenDonVi",oKhachhang->tenDonVi);
    query.bindValue(":mst",oKhachhang->MST);
    if(query.exec()){
        return oKhachhang->KHACHHANGID;
    }else{
        qDebug() << "Khong the them khach hang moi xin kiem tra lai" << query.lastError().text() << query.lastQuery();
        return NULL;
    }
}

const OKhachHang KhachHang::layThongTin(const QString khachHangID)
{
    QSqlQuery query;
    QSharedPointer<OKhachHang> khachHang(new OKhachHang());
    query.exec("SELECT KHACH_HANG.NHOMID,KHACH_HANG.TEN_KHACH_HANG,KHACH_HANG.DIA_CHI_KH FROM KHACH_HANG WHERE KHACHHANGID= '"+khachHangID+"'");
    if(query.next()){
        khachHang.data()->NHOMID = query.value(0).toString();
        khachHang.data()->TEN_KHACH_HANG = query.value(1).toString();
        khachHang.data()->DIA_CHI_KH = query.value(2).toString();
        return *khachHang.data();
    }else{
        qDebug() << "Khong ket qua!";
        return *khachHang.data();
    }
}

bool KhachHang::xoaKhachHang(QString khid)
{
    QSqlQuery query;
    query.exec("delete from KHACH_HANG where KHACHHANGID= '"+khid+"'");
    if(query.numRowsAffected() != -1){
        return true;
    }else{
        qDebug() << "Khong co thong tin xoa "<<query.lastError().text();
        return false;
    }
}


