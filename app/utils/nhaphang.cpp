#include "nhaphang.h"

NhapHang::NhapHang(QObject *parent) : QObject(parent)
{
    taiKhoan = new TaiKhoan();
}

NhapHang::~NhapHang()
{
    delete taiKhoan;
}
/*tra ve id phieu nhap*/
QString NhapHang::themDonNhapHang(OPhieuNhap *ophieuNhap)
{
    QSharedPointer<ManageIndex> taoIndex(new ManageIndex());
    QString indexStr;
    indexStr = taoIndex.data()->getNextIndexCode("PHIEU_NHAP","PN");
    QSqlQuery query;
    QString que = "INSERT INTO PHIEU_NHAP (PHIEUNHAPID, DONDATHANGID, username) "
                  "VALUES ('"+indexStr+"', '"+ ophieuNhap->donDatHangID+"','"+taiKhoan->layIDDangNhapHienTai()+"' )";
    if(query.exec(que)){
        return indexStr;
    } else {
        qDebug() << query.lastError().text();
        //qFatal()<< "Khong the tiep tuc, xin thu lai";
        return "";
    }
}

