#ifndef GIA_H
#define GIA_H

#include <QObject>
#include <Objects/ogia.h>
#include <utils/manageindex.h>

class Gia : public QObject
{
    Q_OBJECT
public:
    explicit Gia(QObject *parent = 0);
    ~Gia();
    bool themGia(OGia *gia);
    QString layGia(const QString hangHoaID);
signals:

public slots:
};

#endif // GIA_H
