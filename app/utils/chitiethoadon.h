#ifndef CHITIETHOADON_H
#define CHITIETHOADON_H

#include <QString>
class ChiTietHoaDon
{
public:
    ChiTietHoaDon(QString hoaDonID);
    bool append(const QString thuocid, QString soLuong, QString donGia);
private:
    QString hoaDonID;
};

#endif // CHITIETHOADON_H
