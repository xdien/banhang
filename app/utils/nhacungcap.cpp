#include "nhacungcap.h"

NhaCungCap::NhaCungCap(QObject *parent) : QObject(parent)
{

}

NhaCungCap::~NhaCungCap()
{

}

bool NhaCungCap::themNCC(NCC *nhacungcap)
{
    QSharedPointer<ManageIndex> taoIndex(new ManageIndex());
    QString indexStr;
    indexStr = taoIndex.data()->getNextIndexCode("NHA_CUNG_CAP","CC");
    QSqlQuery query;
    QString que = "INSERT INTO NHA_CUNG_CAP (NHACUNGCAPID, TEN_NCC, DIA_CHI_NCC) "
                  "VALUES ('"+indexStr+"', '"+nhacungcap->tenNCC+"', '"+nhacungcap->diaChiNCC+"' )";
    if(query.exec(que)){
        return true;
    }else{
        qDebug() << query.lastError().text() << query.lastQuery();
        return false;
    }
}

