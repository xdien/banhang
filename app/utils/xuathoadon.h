#ifndef XUATHOADON_H
#define XUATHOADON_H

#include <QObject>
#include <QSqlQuery>
#include <QSqlError>
#include <QSqlQueryModel>
#include <QDebug>
#include <QStandardItemModel>
#include <QApplication>
#include "lrreportengine.h"
#include "lrcallbackdatasourceintf.h"
class XuatHoaDon : public QObject
{
    Q_OBJECT
public:
    explicit XuatHoaDon(QObject *parent = 0);

    bool themMoi(QStandardItemModel *danhSachChiTietHang);
    QString hdid,username,khid,ngayXuat,mauSo,kyHieu,soHieu,mst,tenKhachHang,tenDonVi,kieuThanhToan,diaChi,trangThai,soTienBangChu,biXoa,daIn,ngayTao;
    QString tongTienHang,tongSoTien,tienThue;
    float thueSuat;
    //ham kiem tra so hieu va ngay
    bool kiemTraLogic(int soHieu, QString ngayXuat);
    void inHoaDon(QString hdid);
signals:

public slots:
private:
    QSqlQuery query;
    QString taoTruyVanThemDanhSach(const QStandardItemModel *danhSachChiTietHang, const QString hoaDonRaId);
};

#endif // XUATHOADON_H
