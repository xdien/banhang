#ifndef HOADON_H
#define HOADON_H

#include <QString>
#include <QSqlQuery>
#include "utils/manageindex.h"
#include "Objects/ohoadon.h"



class HoaDon
{
public:
    HoaDon();
    ~HoaDon();
    QString taoHoaDon(const OHoaDon &hoaDon);
};

#endif // HOADON_H
