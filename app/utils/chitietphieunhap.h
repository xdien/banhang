#ifndef CHITIETPHIEUNHAP_H
#define CHITIETPHIEUNHAP_H

#include <QObject>
#include "Objects/ochitietphieunhap.h"
#include <QStandardItemModel>
#include <QSqlQuery>
#include <QJsonDocument>
#include <QJsonArray>
#include <QDebug>
#include <QSqlError>
class ChiTietPhieuNhap : public QObject
{
    Q_OBJECT
public:
    explicit ChiTietPhieuNhap(QString phieuNhapID, QObject *parent = 0);
    ~ChiTietPhieuNhap();
    QStandardItemModel *layDuDatDangJSON(const QString donDatHangId);
private:
    QString phieuNhapID;
    QList<QStandardItem *> add(const QString &tenThuoc,
                                                            const QString &tenNCC,
                                                            const QString &tenNSX,
                                                            const QString &idNCC,
                                                            const QString &idNSX,
                                                            const QString &soLuong);

signals:

public slots:
};

#endif // CHITIETPHIEUNHAP_H
