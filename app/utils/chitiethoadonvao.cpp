#include "chitiethoadonvao.h"

ChiTietHoaDonVao::ChiTietHoaDonVao(const QString hoaDonVaoID){

    this->hoaDonVaoID = hoaDonVaoID;
}

bool ChiTietHoaDonVao::append(const OChiTietHoaDonvao &oChiTietHoaDonvao)
{
    query.prepare("INSERT INTO CHI_TIET_VAO (HOADONVAOID, HANGHOAID,SO_LUONG,DON_GIA,TONG_SO_TIEN)"
               " VALUES (:hdid,:hhid,:soluong,:dongia,:tongsotien)");
    query.bindValue(":hdid",hoaDonVaoID);
    query.bindValue(":hhid",oChiTietHoaDonvao.HOANGHOAID);
    query.bindValue(":soluong",oChiTietHoaDonvao.SO_LUONG);
    query.bindValue(":dongia",oChiTietHoaDonvao.DON_GIA);
    query.bindValue(":tongsotien",oChiTietHoaDonvao.TONG_SO_TIEN);
    if(query.exec()){
        //neu thanh cong
        return true;
    }else{
        qDebug() << "khong the them chi tiet hang trong hoa don" <<query.lastError().text();
        return false;
    }
}

