#ifndef HOADONRA_H
#define HOADONRA_H
#include "Objects/ohoadonra.h"
#include <QSqlQuery>
#include <QSqlError>
#include "utils/chitiethoadonra.h"
#include <QDebug>

class HoaDonRa
{
public:
    HoaDonRa();
    QString taoMoi(const OHoaDonRa &ohoaDonRa);
    ChiTietHoaDonRa *chiTietHoaDonRa;
private:
    QSqlQuery query;
};

#endif // HOADONRA_H
