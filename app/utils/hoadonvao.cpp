#include "hoadonvao.h"

HoaDonVao::HoaDonVao(QObject *parent) : QObject(parent)
{

}

QString HoaDonVao::taoMoi(const OHoaDonVao &oHoaDonVao)
{
    QSqlQuery query;
    query.setForwardOnly(true);
    query.prepare("INSERT INTO HOA_DON_VAO (HOADONVAOID,USERNAME,"
               "TONG_SO_TIEN,MAU_SO,KY_HIEU, SO_HIEU, NGAY_LAP_HDV, KHACHHANGID) "
               "VALUES (:hdvid,:username,:tongsotien,:mauso,:kyhieu,:sohieu,:ngaylap,:khid)");
    query.bindValue(":hdvid",oHoaDonVao.HOADONVAOID);
    query.bindValue(":username",oHoaDonVao.USERNAME);
    query.bindValue(":tongsotien",oHoaDonVao.tongTienCuoi);
    query.bindValue(":kyhieu",oHoaDonVao.KY_HIEU);
    query.bindValue(":sohieu",oHoaDonVao.SO_HIEU);
    query.bindValue(":ngaylap",oHoaDonVao.NGAY_LAP_HDV);
    query.bindValue(":khid",oHoaDonVao.okhachHang.KHACHHANGID);
    query.bindValue(":mauso",oHoaDonVao.MAU_SO);
    if(!query.exec()){
        //them thanh cong
        qDebug() << "khong the tao moi hoa don" << query.lastError().text();
        return NULL;
    }else {
        return oHoaDonVao.HOADONVAOID;
    }
}

