#ifndef NHOMKHACHHANG_H
#define NHOMKHACHHANG_H

#include <QObject>
#include "Objects/nkh.h"
#include "utils/manageindex.h"
class NhomKhachHang : public QObject
{
    Q_OBJECT
public:
    explicit NhomKhachHang(QObject *parent = 0);
    ~NhomKhachHang();
    bool themNKH(NKH *nkh);
signals:

public slots:
};

#endif // NHOMKHACHHANG_H
