#include "gia.h"

Gia::Gia(QObject *parent) : QObject(parent)
{

}

Gia::~Gia()
{

}

bool Gia::themGia(OGia *gia)
{
    QSharedPointer<ManageIndex> taoIndex(new ManageIndex());
    QString indexStr;
    indexStr = taoIndex.data()->getNextIndexCode("BANG_GIA","BG");
    QSqlQuery query;
    QString que = "INSERT INTO BANG_GIA (GIAID, HANGHOAID, GIA_HANG_HOA, NGAY_AP_DUNG) "
                  "VALUES (:giaid,:hhid, :gia,:ngayApDung );"
                  "update HANG_HOA set GIA_BAN = :gia1 where HANGHOAID = :hhid1";

    query.prepare(que);
    query.bindValue(":giaid",indexStr);
    query.bindValue(":hhid",gia->id);
    query.bindValue(":gia",gia->gia);
    query.bindValue(":hhid1",gia->id);
    query.bindValue(":gia1",gia->gia);
     query.bindValue(":ngayApDung",gia->ngayApDung);
     qDebug() << gia->gia;
    if(query.exec()){
        return true;
    }else{
        qDebug() <<"Loi them gia"<< query.lastError().text() <<que;
        return false;
    }
}

QString Gia::layGia(const QString hangHoaID)
{
    QSqlQuery query;
    QString que = "SELECT BANG_GIA.GIA_HANG_HOA FROM HANG_HOA LEFT JOIN BANG_GIA on BANG_GIA.HANGHOAID = HANG_HOA.HANGHOAID "
                  "WHERE BANG_GIA.NGAY_AP_DUNG in (SELECT max(BANG_GIA.NGAY_AP_DUNG) "
                  "FROM BANG_GIA where HANG_HOA.HANGHOAID = '"+hangHoaID+"' GROUP BY BANG_GIA.HANGHOAID)";
    if(query.exec(que)){
        if(query.next()){
            return query.value(0).toString();
        }else{
            qDebug() << "Khong du lieu!";
            return "Khong du lieu";
        }
    }else{
        qDebug() << query.lastQuery();
        return "false";
    }
}

