#ifndef LOTHUOC_H
#define LOTHUOC_H

#include <QObject>
#include <QSqlQuery>
#include "Objects/olothuoc.h"
#include "manageindex.h"
class LoThuoc : public QObject
{
    Q_OBJECT
public:
    explicit LoThuoc(QObject *parent = 0);
    QString themLoThuoc(OLoThuoc *oLothuoc);

signals:

public slots:
};

#endif // LOTHUOC_H
