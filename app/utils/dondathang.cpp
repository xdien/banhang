#include "dondathang.h"

DonDatHang::DonDatHang(QObject *parent) : QObject(parent)
{
    //dsDatThuoc = new QJsonArray();
    truyVanBangGoiY = "SELECT THUOC.TEN_THUOC,NHA_CUNG_CAP.TEN_NCC,"
                 "NHA_SAN_XUAT.TEN_NSX,NHA_CUNG_CAP.NHACUNGCAPID,NHA_SAN_XUAT.NHASANXUATID "
                 "FROM THUOC LEFT JOIN SO_LO on SO_LO.SO_LO = THUOC.SO_LO "
                 "LEFT JOIN NHA_CUNG_CAP on NHA_CUNG_CAP.NHACUNGCAPID = SO_LO.NHACUNGCAPID "
                 "LEFT JOIN NHA_SAN_XUAT on NHA_SAN_XUAT.NHASANXUATID = SO_LO.NHASANXUATID "
                 "GROUP BY THUOC.TEN_THUOC";
    truyVanNCC = "SELECT NHA_CUNG_CAP.TEN_NCC,NHA_CUNG_CAP.NHACUNGCAPID FROM NHA_CUNG_CAP";
    truyVanNSX = "SELECT NHA_SAN_XUAT.TEN_NSX,NHA_SAN_XUAT.NHASANXUATID FROM NHA_SAN_XUAT";
}

DonDatHang::~DonDatHang()
{
    //delete dsDatThuoc;

}

bool DonDatHang::themDDH(DDH *donDatHang)
{
    QSharedPointer<ManageIndex> taoIndex(new ManageIndex());
    QString indexStr;
    indexStr = taoIndex.data()->getNextIndexCode("DON_DAT_HANG","DH");
    QSqlQuery query;
    QString que = "INSERT INTO DON_DAT_HANG (DONDATHANGID, NO_DDH) "
                  "VALUES ('"+indexStr+"', '"+standarModelToStrJson(donDatHang->noiDung)+"' )";
    if(query.exec(que)){
        return true;
    }else{
        qDebug() << query.lastError().text() << query.lastQuery();
        return false;
    }
}

QString DonDatHang::standarModelToStrJson(const QStandardItemModel *model)
{
    int soDong;
    QJsonArray ar;
    soDong = model->rowCount();
    for(int i = 0;i < soDong; i++){
        QJsonObject dongDuLieuJSON;
        dongDuLieuJSON["ten_thuoc"] = model->index(i,0).data().toString();
        dongDuLieuJSON["ten_ncc"] = model->index(i,1).data().toString();
        dongDuLieuJSON["ten_nsx"] = model->index(i,2).data().toString();
        dongDuLieuJSON["ma_ncc"] = model->index(i,3).data().toString();
        dongDuLieuJSON["ma_nsx"] = model->index(i,4).data().toString();
        dongDuLieuJSON["so_luong"] = model->index(i,5).data().toString();
        ar.append(dongDuLieuJSON);
    }
    QJsonDocument doc(ar);
    QString strJson(doc.toJson(QJsonDocument::Compact));
    return strJson;
}
