#include "hanghoa.h"

HangHoa::HangHoa(QObject *parent) : QObject(parent)
{

}

bool HangHoa::themMoi(const OHangHoa &ohangHoa)
{
    bool kq;
    kq = query.exec("INSERT INTO HANG_HOA (HANGHOAID,DVTID,NHOM_HANG_HOA_ID,TEN_HANG_HOA,DIEN_GIAI, SO_DU)"
                    " VALUES ('"+ohangHoa.HOANGHOAID+"', '"+ohangHoa.DVTID+"', "
                    "'"+ohangHoa.NHOM_HANG_HOA_ID+"', '"+ohangHoa.TEN_HANG_HOA+"', "
                    "'"+ohangHoa.DIEN_GIAI+"', 0)");
    if(kq){
        return true;
    }else{
        qDebug() << query.lastError().text();
        return false;
    }
}

QString HangHoa::layTenHang(const QString &hhid)
{
    query.clear();
    query.prepare("select TEN_HANG_HOA from hang_hoa where HANGHOAID = :hhid");
    query.bindValue(":hhid",hhid);
    query.exec();
    if(query.next()){
        return query.value(0).toString();
    }else{
        return NULL;
    }
}
/*
 * tra ve hanghoaid
*/
QString HangHoa::kiemTraTenHang(const QString &tenHang)
{
    query.clear();
    query.exec("select 	HANGHOAID,DVTID from HANG_HOA where TEN_HANG_HOA = '"+tenHang+"'");
    if(query.next()){
        return query.value(0).toString();
    }else{
        return NULL;
    }
}

bool HangHoa::xoaHang(QString hdid)
{
    query.clear();
    query.exec("delete from HANG_HOA where HANGHOAID  = '"+hdid+"'");
    if(query.numRowsAffected() != -1){
        return true;
    }else{
        qDebug() << "Khong tim thay de xoa "<< query.lastError().text();
        return false;
    }
}

void HangHoa::importTonKho(const QString fileNames, int kyid, int soDongKetThucTrenExcel)
{
    QXlsx::Document xlsx(fileNames);
    xlsx.selectSheet("TamUng");
    //tinh lai luong
    bool trangThaiLoi = true;
    QString nvid;
    for (int i = 8; i < soDongKetThucTrenExcel; ++i) {
        query.clear();
        query.prepare("insert into TAM_UNGD1(KYID,NHANVIENID,SO_TIEN) "
                      "values(:kyid,:nvid,:soTien)");
        query.bindValue(":kyid",kyid);
        nvid = xlsx.read(i,3).toString();
        query.bindValue(":nvid",nvid);//ma so nhan vien tren excel
        QXlsx::Cell *cell=xlsx.cellAt(i, 6);
        if (cell){
            query.bindValue(":soTien",cell->value().toInt());//so tien cua nhan vien tren excel
            if(!query.exec()){
                qDebug() << "Loi import tam ung d1 " << nvid << kyid;
            }else{
                //                trangThaiLoi = layThongTin(nvid,kyid);
                //                if(trangThaiLoi)//lay thong tin cua phieu luong that bai = chua cham cong
                //                {
                //                    this->ckgtTud1 = cell->value().toInt();
                //                    m_nvid = nhanvienid = nvid;
                //                    m_kyid = kyid;
                //                    this->kyid = kyid;//cho ham update
                //                    tuTinh();
                //                    if(!update())
                //                        qDebug() << "Khong the cap nhat thong tin thuong cho nhan vien";
                //                    else{
                //                        qDebug() << "Cap nhat thanh cong" << m_nvid;
                //                    }
                //                }else{
                //                    qDebug() << "Khong lấy được thông tin ";
                //                }
            }
        }
    }
}

