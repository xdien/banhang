#ifndef THUOC_H
#define THUOC_H

#include <QObject>
#include "Objects/othuoc.h"
#include <QSqlQuery>
#include <QSqlError>
#include "utils/manageindex.h"
class Thuoc : public QObject
{
    Q_OBJECT
public:
    explicit Thuoc(QObject *parent = 0);
    ~Thuoc();
    bool themThuoc(OThuoc *oThuoc);
    OThuoc timThuoc(const QString tenThuoc);
signals:

public slots:
};

#endif // THUOC_H
