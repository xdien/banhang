#include "chitiethoadon.h"

#include <QSharedPointer>
#include <QSqlQuery>
#include <QDebug>
#include <QSqlError>
#include "math.h"

ChiTietHoaDon::ChiTietHoaDon(QString hoaDonID)
{
    this->hoaDonID = hoaDonID;
}

bool ChiTietHoaDon::append(const QString thuocid, const QString soLuong, QString donGia)
{
    QSqlQuery query;
    QString que = "INSERT INTO CHI_TIET_HOA_DON(HOADONID,THUOCID,SO_LUONG, DON_GIA) "
                  "VALUES ('"+hoaDonID+"', '"+thuocid+"', '"+soLuong+"', '"+donGia+"')";

    if(query.exec(que)){
        /*tru so du trong bang THUOC*/
        query.exec("select THUOC.SO_DU FROM THUOC where THUOC.THUOCID ='"+thuocid+"'");
        if(query.next()){
            int soDu = query.value(0).toInt(0);
            soDu = soDu - soLuong.toInt(0);
            query.exec("UPDATE THUOC SET SO_DU = '"+QString::number(soDu)+"' WHERE THUOC.THUOCID = '"+thuocid+"'");
            return true;
        }else {
            return false;
        }

    }else{
        qDebug() << query.lastError().text();
        return false;
    }
}

