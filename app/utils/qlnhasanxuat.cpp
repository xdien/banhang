#include "qlnhasanxuat.h"

QLNhaSanXuat::QLNhaSanXuat(QObject *parent) : QObject(parent)
{

}

QLNhaSanXuat::~QLNhaSanXuat()
{
    qDebug() << "ham delete QLNhaSanXuat ";
}

bool QLNhaSanXuat::themNSX(NSX *nhasx)
{
    QSharedPointer<ManageIndex> taoIndex(new ManageIndex());
    QString indexStr;
    indexStr = taoIndex.data()->getNextIndexCode("NHA_SAN_XUAT","SX");
    QSqlQuery query;
    QString que = "INSERT INTO NHA_SAN_XUAT (NHASANXUATID, QUOC_GIA, TEN_NSX, _IA_CHI_NSX) "
                  "VALUES ('"+indexStr+"', '"+ nhasx->quocGiaNSX+"', '"+nhasx->tenNSX+"', '"+nhasx->diaChiNSX+"' )";
    if(query.exec(que)){
        return true;
    }else{
        qDebug() << "Khong the them nha san xuat! " <<query.lastError().text();
        return false;
    }
}

QCompleter *QLNhaSanXuat::layDanhSachQuocGia()
{
    QStringList wordList;
    wordList << "vn - Viet Nam" << "US - My" << "cn - Trung Quoc" << "jb - Nhat";
    QCompleter *completer = new QCompleter(wordList);
    return completer;
}

