#ifndef CHITIETHOADONRA_H
#define CHITIETHOADONRA_H
#include <QSqlQuery>
#include <QString>
#include "Objects/ochitiethoadonra.h"
#include "Objects/ohoadonra.h"
#include <QDebug>

class ChiTietHoaDonRa
{
public:
    ChiTietHoaDonRa(const QString &hoaDonRaID);
    bool append(const OChiTietHoaDonRa &ochiTietHoaDonRa);
private:
    QString hoaDonRaID;
    QSqlQuery query;
};

#endif // CHITIETHOADONRA_H
