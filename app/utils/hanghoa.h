#ifndef HANGHOA_H
#define HANGHOA_H

#include <QObject>
#include "Objects/ohanghoa.h"
#include <QSqlError>
#include <QDebug>
#include <QSqlQuery>
#include <QtXlsx>

class HangHoa : public QObject
{
    Q_OBJECT
public:
    explicit HangHoa(QObject *parent = 0);
    bool themMoi(const OHangHoa &ohangHoa);
    QString layTenHang(const QString &hhid);
    QString kiemTraTenHang(const QString &tenHang);
    bool xoaHang(QString hdid);
    void importTonKho(const QString fileNames, int kyid, int soDongKetThucTrenExcel);
private:
    QSqlQuery query;
signals:

public slots:
};

#endif // HANGHOA_H
