#ifndef TAIKHOAN_H
#define TAIKHOAN_H

#include <QObject>
#include "Objects/otaikhoan.h"
#include <QSqlQuery>
#include "utils/manageindex.h"

class TaiKhoan : public QObject
{
    Q_OBJECT
public:
    explicit TaiKhoan(QObject *parent = 0);
    ~TaiKhoan();
    bool themTaiKhoan(OTaiKhoan *otaiKhoan);
    QString layIDDangNhapHienTai();
    bool danNhap(const QString userName, const QString pass);
signals:
    void thongBaoDangNhap(QString str, QString tenNV);
private:
    QString standarModelToStrJson(const QStandardItemModel *model);
signals:

public slots:
};

#endif // TAIKHOAN_H
