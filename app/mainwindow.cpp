#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "dialog_setting.h"
#include <QMessageBox>


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{

    ui->setupUi(this);
    db = QSqlDatabase::database("qt_sql_default_connection");
    //ui->menuBao_cao->setEnabled(false);
    ui->menuHe_thong->setEnabled(true);//Comment de bao tri
    /*Thanh Thong bao trang thai status bar*/
    statusLabel = new QLabel(this);
    tinhtrangsql = new QLabel(this);
    statusLabel->setText("Bạn chưa đăng nhập");
    ui->statusBar->addPermanentWidget(tinhtrangsql);
    ui->statusBar->addPermanentWidget(statusLabel);
    if(db.isOpen())
    {
        /*Khoi dong cac form*/
        moi = new login();
        connect(moi,SIGNAL(nvdangnhap(QString)),this,SLOT(capnhatPhanQuyen(QString)));
        moi->exec();
        tinhtrangsql->setText("Thiết lập kết nối thành công |");
    }else{
        ui->actionDan_nhap->setEnabled(false);
        tinhtrangsql->setText("Không thể kết nối tới máy chủ, xem lại tùy chỉnh kết nối");
    }

}

MainWindow::~MainWindow()
{
    delete ui;
    delete moi;
    delete statusLabel;
    delete tinhtrangsql;
}

bool MainWindow::timQuyen(QString formid)
{
    for (int i = 0; i < ar.size(); ++i) {
        if(ar[i].toObject()["formid"].toString() == formid){
            return true;
        }
    }
    return false;
}

void MainWindow::on_actionSQL_triggered()
{
    Dialog_setting setting;
    setting.exec();
}

void MainWindow::on_actionDan_nhap_triggered()
{
    if(!moi->isActiveWindow())
    {
        moi->show();
        moi->activateWindow();
    }
}

/*void MainWindow::on_actionPhieu_DK_triggered()
{
    if(!modangkyphieutiem->isActiveWindow())
    {
        ui->mdiArea->addSubWindow(modangkyphieutiem);
    }
     modangkyphieutiem->showMaximized();
}
*/


void MainWindow::on_actionAbout_Qt_triggered()
{
    QMessageBox aa;
    QString aas ="Thong tin ve Qt";
    aa.setAttribute(Qt::WA_DeleteOnClose,true);
    aa.aboutQt(parentWidget(), aas);
    aa.show();
}
void MainWindow::capnhatPhanQuyen(QString username)//cap nhat phan quyen dua theo macv
{
    QJsonDocument doc;
    query.exec("select coquyen from TAI_KHOAN where username = '"+username+"'");
    if(query.next()){
        doc = QJsonDocument::fromJson(query.value(0).toString().toUtf8());
        ar = doc.array();
        statusLabel->setText("Xin chào! " + username);
    }
}


void MainWindow::on_actionAbout_triggered()
{
    about *a = new about();
    a->setAttribute(Qt::WA_DeleteOnClose,true);
    a->show();
}

void MainWindow::on_actionBan_triggered()
{
    //Kiem tra doi tuong xem co bi xoa vung nho chua
    //http://stackoverflow.com/questions/65724/uninitialized-memory-blocks-in-vc
    /*FormBanThuoc *banThuoc = new FormBanThuoc();
    banThuoc->setAttribute(Qt::WA_DeleteOnClose,true);
    if(banThuoc != NULL){
        if(!banThuoc->isActiveWindow())
        {
            ui->mdiArea->addSubWindow(banThuoc);
        }
        banThuoc->showMaximized();
    }else{
        qDebug() << "banThuoc da bi xoa!, tao lai";
        banThuoc = new FormBanThuoc();
        ui->mdiArea->addSubWindow(banThuoc);
        banThuoc->showMaximized();
    }*/
    QPointer<XuatHoaDonForm> xuatHoaDonf;
    xuatHoaDonf = new XuatHoaDonForm();
    xuatHoaDonf->setAttribute(Qt::WA_DeleteOnClose,true);
    xuatHoaDonf->show();
}


void MainWindow::on_actionQuan_ly_NKH_triggered()
{
    FormNhomKhachHang *formNkh = new  FormNhomKhachHang();
    formNkh->setAttribute(Qt::WA_DeleteOnClose,true);
    if(timQuyen(formNkh->metaObject()->className())){
        formNkh->show();
    }else{
        qDebug() << "Chua cap quyen!";
    }
}


void MainWindow::on_actionQuyen_triggered()
{
    FormQuyen *FQuyen = new FormQuyen();
    /*if(timQuyen(FQuyen->metaObject()->className())){
        FQuyen->show();
    }else{
        qDebug() << "Chua cap quyen!";
    }*/
    FQuyen->setAttribute(Qt::WA_DeleteOnClose,true);
    FQuyen->show();
}

void MainWindow::on_actionTai_Khoan_triggered()
{
    FormTaiKhoan *FTaiKHoan = new FormTaiKhoan();
    /*if(timQuyen(FTaiKHoan->metaObject()->className())){
        FTaiKHoan->show();
    }else{
        qDebug() << "Chua cap quyen!";
    }*/
    FTaiKHoan->setAttribute(Qt::WA_DeleteOnClose,true);
    FTaiKHoan->show();
}

void MainWindow::on_actionPhieu_chi_triggered()
{
   FormPhieuChi * phieuCHi = new FormPhieuChi();
   phieuCHi->setAttribute(Qt::WA_DeleteOnClose,true);
   if(timQuyen(phieuCHi->metaObject()->className())){
       phieuCHi->show();
   }else{
       qDebug() << "Chua cap quyen!";
   }
}


void MainWindow::on_actionKhach_Hang_triggered()
{
    DialogKhachHang *fKhachHang = new DialogKhachHang();
    fKhachHang->setAttribute(Qt::WA_DeleteOnClose,true);
    if(timQuyen(fKhachHang->metaObject()->className())){
        fKhachHang->show();
    }else{
        qDebug() << "Chua cap quyen!";
    }
}

void MainWindow::on_actionGia_triggered()
{
    FormGia *giaf = new FormGia();
    giaf->setAttribute(Qt::WA_DeleteOnClose,true);
    if(timQuyen(giaf->metaObject()->className())){
        giaf->show();
    }else{
        qDebug() << "Chua cap quyen!";
    }
}


void MainWindow::on_actionH_a_n_triggered()
{
    BCDauRa *baoCao;
    baoCao = new BCDauRa();
    baoCao->setAttribute(Qt::WA_DeleteOnClose,true);
    baoCao->show();
}


void MainWindow::on_actionNhapHang_2_triggered()
{
    FormDauVao *fdauVao = new FormDauVao();
    fdauVao->setAttribute(Qt::WA_DeleteOnClose,true);
    fdauVao->show();
}

void MainWindow::on_actionNhapHang_triggered()
{
    FormDauVao *fdauVao = new FormDauVao();
  fdauVao->setAttribute(Qt::WA_DeleteOnClose,true);
    if(!fdauVao->isActiveWindow())
    {
        ui->mdiArea->addSubWindow(fdauVao);
    }
     fdauVao->showMaximized();
}

void MainWindow::on_actionNhom_Hang_Hoa_triggered()
{
   FormNhomHangHoa *fnhom = new FormNhomHangHoa();
   fnhom->setAttribute(Qt::WA_DeleteOnClose,true);
   fnhom->show();
}

void MainWindow::on_actionHang_Hoa_triggered()
{
    FormHangHoa *fhangHoa = new FormHangHoa();
    fhangHoa->setAttribute(Qt::WA_DeleteOnClose,true);
    fhangHoa->show();
}

void MainWindow::on_actionTon_kho_triggered()
{
    BCTonKho *bcTonKHo = new BCTonKho();
    bcTonKHo->setAttribute(Qt::WA_DeleteOnClose,true);
    bcTonKHo->show();
}

void MainWindow::on_actionSua_mau_hd_triggered()
{
    QScopedPointer<LimeReport::ReportEngine> report;
    report.reset(new LimeReport::ReportEngine(this));
    report->designReport();
}
