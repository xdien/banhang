#include "formchitietphieunhap.h"
#include "ui_formchitietphieunhap.h"

FormChiTietPhieuNhap::FormChiTietPhieuNhap(QString datHangId, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::FormChiTietPhieuNhap)
{
    ui->setupUi(this);
    this->donDatHangID = datHangId;
    chiTiet = new ChiTietPhieuNhap(datHangId);
    danhSachDat = new QStandardItemModel();
    danhSachhangHienTai = new QStandardItemModel();
    ui->treeViewDatHang->setModel(chiTiet->layDuDatDangJSON(datHangId));
    danhSachDat = chiTiet->layDuDatDangJSON(donDatHangID);
    ui->tableViewHangHienTai->setModel(danhSachhangHienTai);
    nhapHang = new NhapHang();
    thuoc = new Thuoc();
    setWindowTitle("Chi tiết phiếu nhập ["+QString::fromLatin1(metaObject()->className())+"]");

}

FormChiTietPhieuNhap::~FormChiTietPhieuNhap()
{
    delete ui;
    delete danhSachDat;
    delete danhSachhangHienTai;
    delete chiTiet;
    delete thuoc;
}

void FormChiTietPhieuNhap::on_pushButton_3_clicked()
{
    QString phieuNhapID;
    QSqlQuery query;
    /*can phieuChiID + donHangID*/
    QSharedPointer<OPhieuNhap> oPhieuNhap(new OPhieuNhap());
    oPhieuNhap.data()->donDatHangID = donDatHangID;
    phieuNhapID = nhapHang->themDonNhapHang(oPhieuNhap.data());
    /*Duyet tat ca danh sach tu danhSachhangHienTai luu tung dong vao bang chi_tiet_nhap*/
    int soDong = danhSachhangHienTai->rowCount();
    for (int i = 0; i < soDong; ++i) {
        /*them lo thuoc*/
        query.exec("INSERT INTO SO_LO (SO_LO, NHACUNGCAPID, NHASANXUATID, NGAY_SAN_XUAT, NGAY_HET_HAN) "
                   "VALUES ('"+danhSachhangHienTai->index(i,1).data().toString()+"', '"+danhSachhangHienTai->index(i,6).data().toString()+"', "
                   "'"+danhSachhangHienTai->index(i,7).data().toString()+"', "
                   "'"+danhSachhangHienTai->index(i,4).data().toString()+"', '"+danhSachhangHienTai->index(i,5).data().toString()+"' )");
        qDebug() << query.lastQuery();
        /*them vao chi tiet nhap voi soLo*/
        query.exec("INSERT INTO CHI_TIET_NHAP (PHIEUNHAPID, SO_LO, SO_LUONG_NHAP, DON_GIA_NHAP) "
                   "VALUES ('"+phieuNhapID+"', '"+danhSachhangHienTai->index(i,1).data().toString()+"', "
                   "'"+danhSachhangHienTai->index(i,3).data().toString()+"', "
                   "'"+danhSachhangHienTai->index(i,2).data().toString()+"')");
        /*them thuoc moi*/
        QSharedPointer<OThuoc> othuo(new OThuoc());
        othuo.data()->SO_LO = danhSachhangHienTai->index(i,1).data().toString();
        othuo.data()->TEN_THUOC = danhSachhangHienTai->index(i,0).data().toString();
        othuo.data()->SO_DU = danhSachhangHienTai->index(i,3).data().toString();
        thuoc->themThuoc(othuo.data());
        qDebug() << query.lastError().text()<< query.lastQuery();
    }
}

void FormChiTietPhieuNhap::on_pushButton_clicked()
{
    danhSachhangHienTai->appendRow(add(ui->lineEditTenThuoc->text(),
                                       ui->lineEditSolo->text(),
                                       ui->lineEditGia->text(),
                                       ui->lineEditSoLuong->text(),
                                       ui->dateEditNgaySX->date().toString(QString::fromLatin1(FORMAT_DATE)),
                                       ui->dateEditHetHan->date().toString(QString::fromLatin1(FORMAT_DATE)),
                                       ui->lineEditNCC->text(),
                                       ui->lineEditNSX->text()));
}
QList<QStandardItem *> FormChiTietPhieuNhap::add(const QString &tenThuoc,
                                                 const QString &soLo,
                                                 const QString &gia,
                                                 const QString &soLuong,
                                                 const QString &ngaySX,
                                                 const QString &ngayHetHan,
                                                 const QString &ncc,
                                                 const QString &nsx)
{
    QList<QStandardItem *> rowItems;
    rowItems << new QStandardItem(tenThuoc);
    rowItems << new QStandardItem(soLo);
    rowItems << new QStandardItem(gia);
    rowItems << new QStandardItem(soLuong);
    /*them thong tin nhap vao bang thuoc*/
    rowItems << new QStandardItem(ngaySX);
    rowItems << new QStandardItem(ngayHetHan);
    rowItems << new QStandardItem(ncc);
    rowItems << new QStandardItem(nsx);

    return rowItems;
}

void FormChiTietPhieuNhap::on_treeViewDatHang_clicked(const QModelIndex &index)
{
    ui->lineEditTenThuoc->setText(danhSachDat->index(index.row(),0).data().toString());
    ui->lineEditSoLuong->setText(danhSachDat->index(index.row(),5).data().toString());
    ui->lineEditNSX->setText(danhSachDat->index(index.row(),4).data().toString());
    ui->lineEditNCC->setText(danhSachDat->index(index.row(),3).data().toString());
    /*Hien thi form nhap so lo*/

}

void FormChiTietPhieuNhap::on_lineEditSolo_textChanged(const QString &arg1)
{

}
