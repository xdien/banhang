#ifndef FORMHANGHOA_H
#define FORMHANGHOA_H

#include <QWidget>
#include <QSqlQueryModel>
#include <QSqlError>
#include <QCompleter>
#include <QDebug>
#include "utils/donvitinh.h"
#include <QMessageBox>
#include <QSqlTableModel>
#include "utils/hanghoa.h"
#include <QMenu>
#include "utils/manageindex.h"
#include "utils/donvitinh.h"
#include "chucnang/formnhomhanghoa.h"

namespace Ui {
class FormHangHoa;
}

class FormHangHoa : public QWidget
{
    Q_OBJECT

public:
    explicit FormHangHoa(QWidget *parent = 0);
    ~FormHangHoa();

private slots:
    void on_pushButton_clicked();

    void on_toolButton_clicked();
    void customMenuRequested(QPoint pos);
    void xoaHang();

    void on_pushButtonLuu_clicked();

    void on_toolButtonimportExcel_clicked();

private:
    Ui::FormHangHoa *ui;
    QCompleter *completer;
    QSqlQueryModel *donvitinhModel;
    QSqlTableModel *hangHoaModel, *nhomHangHoaModel;
    HangHoa *hangHoa;
    DonViTinh *donViTinh;

    QMenu *contextMenu;
    QAction *action_Xoa;
};

#endif // FORMHANGHOA_H
