#ifndef FORMQUYEN_H
#define FORMQUYEN_H

#include <QWidget>
#include "utils/quyen.h"

namespace Ui {
class FormQuyen;
}

class FormQuyen : public QWidget
{
    Q_OBJECT

public:
    explicit FormQuyen(QWidget *parent = 0);
    ~FormQuyen();
    Quyen *quyen;
private slots:
    void on_pushButton_2_clicked();

private:
    Ui::FormQuyen *ui;
    QSqlTableModel *tableModelQuyen;
    void xoaQuyen(QAction *id);
};

#endif // FORMQUYEN_H
