#include "formnhomhanghoa.h"
#include "ui_formnhomhanghoa.h"

FormNhomHangHoa::FormNhomHangHoa(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::FormNhomHangHoa)
{
    ui->setupUi(this);
    nhomHangHoaModel = new QSqlTableModel();
    cbnhomHangHoa = new QSqlQueryModel();
    nhomHangHoaModel->setTable("NHOM_HANG_HOA");
    nhomHangHoaModel->setEditStrategy(QSqlTableModel::OnRowChange);
    ui->tableView->setModel(nhomHangHoaModel);
    nhomHangHoa = new NhomHangHoa();
    ui->comboBoxNhom->setModel(nhomHangHoaModel);
    lamMoi();
}

FormNhomHangHoa::~FormNhomHangHoa()
{
    delete ui;
    delete nhomHangHoaModel;
    delete cbnhomHangHoa;
}


void FormNhomHangHoa::on_pushButtonThemNhom_clicked()
{
    if(nhomHangHoa->themNhom(ONhomHangHoa(ui->lineEditTenNhom->text(),ui->comboBoxNhom->currentText(),ui->textEditMota->document()->toPlainText()))){
        nhomHangHoaModel->select();
        emit sukienThem();
    }
}

void FormNhomHangHoa::lamMoi()
{
    nhomHangHoaModel->select();
}
