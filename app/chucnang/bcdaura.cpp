#include "bcdaura.h"
#include "ui_bcdaura.h"

BCDauRa::BCDauRa(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::BCDauRa)
{
    dsHangModel = new QSqlQueryModel();
    ui->setupUi(this);
    ui->tableView->setModel(dsHangModel);
    dsHangModel->setQuery("select HOADONRAID,TONG_SO_TIEN,USERNAME,sum(TONG_SO_TIEN) from HOA_DON_RA where date(NGAY_LAP) = CURRENT_DATE");
    this->layDL();
}

BCDauRa::~BCDauRa()
{
    delete ui;
    delete dsHangModel;
}

void BCDauRa::on_dateEditTu_dateChanged(const QDate &date)
{
    ui->dateEditDen->setMinimumDate(date);
}

void BCDauRa::layDL()
{
    if(dsHangModel->rowCount()>0){
        ui->lineEditTongTien->setText(dsHangModel->index(0,3).data().toString());
    }
}


void BCDauRa::on_pushButton_2_clicked()
{
    dsHangModel->setQuery("select HOADONRAID,TONG_SO_TIEN,USERNAME,sum(TONG_SO_TIEN) from HOA_DON_RA "
                          "where date(NGAY_LAP) > '"+ui->dateEditTu->date().toString("yyyy-MM-dd")+"' and "
                          "date(NGAY_LAP) <='"+ui->dateEditDen->date().toString("yyyy-MM-dd")+"'");
    layDL();

}


void BCDauRa::on_pushButtonTest_clicked()
{
   LimeReport::ReportEngine *report = new LimeReport::ReportEngine(this);
     //report->dataManager()->addModel("string_list",stringListModel,true);
     report->loadFromFile(QApplication::applicationDirPath()+"/reports/baocaotonkho.lrxml");
     qDebug()<< report->lastError();
     LimeReport::PreviewReportWidget* m_preview = report->createPreviewWidget();
     //m_preview->refreshPages();
     report->previewReport();
}
