#ifndef FORMNHOMHANGHOA_H
#define FORMNHOMHANGHOA_H

#include <QWidget>
#include <QSqlTableModel>
#include <QSqlQueryModel>
#include "utils/nhomhanghoa.h"

namespace Ui {
class FormNhomHangHoa;
}

class FormNhomHangHoa : public QWidget
{
    Q_OBJECT

public:
    explicit FormNhomHangHoa(QWidget *parent = 0);
    ~FormNhomHangHoa();
signals:
    void sukienThem();
private slots:
    void on_pushButtonThemNhom_clicked();

private:
    Ui::FormNhomHangHoa *ui;
    QSqlTableModel *nhomHangHoaModel;
    NhomHangHoa *nhomHangHoa;
    QSqlQueryModel *cbnhomHangHoa;
    void lamMoi();
};

#endif // FORMNHOMHANGHOA_H
