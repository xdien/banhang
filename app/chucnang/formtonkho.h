#ifndef FORMTONKHO_H
#define FORMTONKHO_H

#include <QWidget>

namespace Ui {
class FormTonKho;
}

class FormTonKho : public QWidget
{
    Q_OBJECT

public:
    explicit FormTonKho(QWidget *parent = 0);
    ~FormTonKho();

private:
    Ui::FormTonKho *ui;
};

#endif // FORMTONKHO_H
