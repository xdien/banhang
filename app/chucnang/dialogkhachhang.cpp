#include "dialogkhachhang.h"
#include "ui_dialogkhachhang.h"

DialogKhachHang::DialogKhachHang(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DialogKhachHang)
{
    ui->setupUi(this);
    khachHang = new KhachHang();
    contextMenu = new QMenu(this);
    action_Xoa = new QAction("Xóa");
    contextMenu->addAction(action_Xoa);
    /*kieu du lieu trong model cot 0 -> ten nhom(mo ta) 1->nhomid*/
    cbNhomKhachhangModel = new QSqlQueryModel();
    ui->comboBoxNhom->setModel(cbNhomKhachhangModel);
    khachHangModel = new QSqlRelationalTableModel();
    cbNhomKhachhangModel->setQuery("select MO_TA_NHOM,NHOMID from NHOM_KHACH_HANG");
    ui->tableViewKH->setModel(khachHangModel);
    khachHangModel->setTable("KHACH_HANG");
    khachHangModel->setRelation(1,QSqlRelation("NHOM_KHACH_HANG","NHOMID","MO_TA_NHOM"));
    khachHangModel->select();
    khachHangModel->setEditStrategy(QSqlTableModel::OnManualSubmit);
    setWindowTitle("Khách Hàng ["+QString::fromLatin1(metaObject()->className())+"]");
    ui->lineEditMaKH->setEnabled(false);
    taoIndex = new ManageIndex();
    ui->lineEditMaKH->setText(taoIndex->getNextIndexCode("KHACH_HANG","KH"));
    ui->tableViewKH->horizontalHeader()->setResizeContentsPrecision(QHeaderView::ResizeToContents);
    ui->tableViewKH->verticalHeader()->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(ui->tableViewKH->verticalHeader(),SIGNAL(customContextMenuRequested(QPoint)),this,SLOT(customMenuRequested(QPoint)));
    connect(action_Xoa,SIGNAL(triggered(bool)),this,SLOT(xoaKhachHang()));
}

DialogKhachHang::~DialogKhachHang()
{
    delete ui;
    delete khachHang;
    delete cbNhomKhachhangModel;
    delete khachHangModel;
    delete taoIndex;
    delete action_Xoa;
    delete contextMenu;
}

void DialogKhachHang::on_pushButton_clicked()
{
    QSharedPointer<OKhachHang> okhachhang(new OKhachHang());
    okhachhang.data()->TEN_KHACH_HANG = ui->lineEditTen->text();
    okhachhang.data()->DIA_CHI_KH = ui->plainTextEditDc->document()->toPlainText();
    okhachhang.data()->KHACHHANGID = ui->lineEditMaKH->text();
    okhachhang.data()->MST = ui->lineEditMST->text();
    okhachhang.data()->tenDonVi = ui->lineEditTenDonVienDonVi->text();
    okhachhang.data()->NHOMID = cbNhomKhachhangModel->index(ui->comboBoxNhom->currentIndex(),1).data().toString();
    if(khachHang->themKhachHang(okhachhang.data())!= NULL){
        khachHangModel->select();
    }
}

void DialogKhachHang::on_toolButtonThemNhom_clicked()
{
    FormNhomKhachHang *nhomkh = new FormNhomKhachHang();
    connect(nhomkh,SIGNAL(lamMoi()),this,SLOT(lamMoiCb()));
    nhomkh->show();
}
void DialogKhachHang::lamMoiCb()
{
    cbNhomKhachhangModel->setQuery("select MO_TA_NHOM,NHOMID from NHOM_KHACH_HANG");
}

void DialogKhachHang::on_checkBox_clicked(bool checked)
{
    if(checked){
        ui->lineEditMaKH->clear();
        ui->lineEditMaKH->setEnabled(checked);
    }else{
        ui->lineEditMaKH->setEnabled(false);
        ui->lineEditMaKH->setText(taoIndex->getNextIndexCode("KHACH_HANG","KH"));
    }
}

void DialogKhachHang::on_pushButton_Luu_clicked()
{
    khachHangModel->submit();
}
void DialogKhachHang::customMenuRequested(QPoint pos)
{
    contextMenu->popup(ui->tableViewKH->viewport()->mapToGlobal(pos));
    ui->tableViewKH->setCurrentIndex(ui->tableViewKH->indexAt(pos));
}

void DialogKhachHang::xoaKhachHang()
{
    if(khachHang->xoaKhachHang(khachHangModel->index(ui->tableViewKH->currentIndex().row(),0).data().toString()))
        khachHangModel->select();
    else
        QMessageBox::warning(this,"Lỗi không xóa được khách hàng", "Bạn hãy thử lại, or kiểm tra lại mạng",QMessageBox::Ok,QMessageBox::NoButton);
}
