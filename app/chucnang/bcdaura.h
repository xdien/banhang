#ifndef BCDAURA_H
#define BCDAURA_H

#include <QWidget>
#include <QDebug>
#include <QSqlQueryModel>
#include "lrreportengine.h"
#include "lrcallbackdatasourceintf.h"

namespace Ui {
class BCDauRa;
}

class BCDauRa : public QWidget
{
    Q_OBJECT

public:
    explicit BCDauRa(QWidget *parent = 0);
    ~BCDauRa();

private slots:
    void on_dateEditTu_dateChanged(const QDate &date);

    void on_pushButton_2_clicked();

    void on_pushButtonTest_clicked();

private:
    Ui::BCDauRa *ui;
    QSqlQueryModel *dsHangModel;
    void layDL();
};

#endif // BCDAURA_H
