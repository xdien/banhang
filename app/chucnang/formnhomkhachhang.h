#ifndef FORMNHOMKHACHHANG_H
#define FORMNHOMKHACHHANG_H

#include <QWidget>
#include <utils/nhomkhachhang.h>

namespace Ui {
class FormNhomKhachHang;
}

class FormNhomKhachHang : public QWidget
{
    Q_OBJECT

public:
    explicit FormNhomKhachHang(QWidget *parent = 0);
    ~FormNhomKhachHang();

private:
    Ui::FormNhomKhachHang *ui;
    QSqlTableModel *nkhModel;
    NhomKhachHang *nkh;
signals:
    void lamMoi();
private slots:
    void customMenuRequested(QPoint pos);
    void xoaNKH(QAction *id);
    void on_buttonBox_accepted();
};

#endif // FORMNHOMKHACHHANG_H
