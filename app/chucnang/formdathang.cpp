#include "formdathang.h"
#include "ui_formdathang.h"

FormDatHang::FormDatHang(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::FormDatHang)
{
    ui->setupUi(this);
    ddh = new DonDatHang();
    cbNCCModel = new QSqlQueryModel();
    cbNSXModel = new QSqlQueryModel();
    modelDanhSachGoiY = new QSqlQueryModel();
    //modelDanhSachDuocChon = new QStandardItemModel();
    modelDanhSachGoiY->setQuery(ddh->truyVanBangGoiY);
    cbNCCModel->setQuery(ddh->truyVanNCC);
    cbNSXModel->setQuery(ddh->truyVanNSX);
    ui->comboBoxNCC->setModel(cbNCCModel);
    ui->comboBoxNSX->setModel(cbNSXModel);
    ui->treeViewThuoc->setModel(modelDanhSachGoiY);
    ui->treeViewThuocDuocChon->setModel(&modelDanhSachDuocChon);
    /*modelDanhSachDuocChon->setHeaderData(0,Qt::Horizontal,"Ten Thuoc");
    modelDanhSachDuocChon->setHeaderData(1,Qt::Horizontal,"Ten Thuoc");
    modelDanhSachDuocChon->setHeaderData(2,Qt::Horizontal,"Ten Thuoc");
    modelDanhSachDuocChon->setHeaderData(3,Qt::Horizontal,"Ten Thuoc");
    modelDanhSachDuocChon->setHeaderData(4,Qt::Horizontal,"Ten Thuoc");*/
    setWindowTitle("Đặt Hàng ["+QString::fromLatin1(metaObject()->className())+"]");
    lamMoiNCC();
    lamMoiNSX();

}

FormDatHang::~FormDatHang()
{
    delete ui;
    //delete modelDanhSachDuocChon;
    delete modelDanhSachGoiY;
    delete cbNCCModel;
    delete cbNSXModel;
}

QList<QStandardItem *> FormDatHang::dongKhungThuoc(const QString &tenThuoc,
                                                        const QString &tenNCC,
                                                        const QString &tenNSX,
                                                        const QString &idNCC,
                                                        const QString &idNSX,
                                                        const QString &soLuong)
{
    QList<QStandardItem *> rowItems;
    rowItems << new QStandardItem(tenThuoc);
    rowItems << new QStandardItem(tenNCC);
    rowItems << new QStandardItem(tenNSX);
    rowItems << new QStandardItem(idNCC);
    rowItems << new QStandardItem(idNSX);
    rowItems << new QStandardItem(soLuong);
    return rowItems;
}

void FormDatHang::on_treeViewThuoc_clicked(const QModelIndex &index)
{

    if(modelDanhSachDuocChon.findItems(modelDanhSachGoiY->index(index.row(),0).data().toString(),Qt::MatchExactly,0).count()==0){
        modelDanhSachDuocChon.appendRow(dongKhungThuoc(modelDanhSachGoiY->index(index.row(),0).data().toString(),
                                                                                  modelDanhSachGoiY->index(index.row(),1).data().toString(),
                                                                                  modelDanhSachGoiY->index(index.row(),2).data().toString(),
                                                                                  modelDanhSachGoiY->index(index.row(),3).data().toString(),
                                                                                  modelDanhSachGoiY->index(index.row(),4).data().toString(),
                                                       "1"));
    }

}

void FormDatHang::on_pushButtonThem_clicked()
{
    //modelDanhSachDuocChon.appendRow();
    modelDanhSachDuocChon.appendRow(dongKhungThuoc(ui->lineEditTenThuoc->text(),
                                                                              cbNCCModel->index(ui->comboBoxNCC->currentIndex(),0).data().toString(),
                                                                              cbNSXModel->index(ui->comboBoxNSX->currentIndex(),0).data().toString(),
                                                                              cbNCCModel->index(ui->comboBoxNCC->currentIndex(),1).data().toString(),
                                                                              cbNSXModel->index(ui->comboBoxNSX->currentIndex(),1).data().toString(),
                                                   ui->spinBoxSoLuong->text()));
}

void FormDatHang::on_toolButtonThemNCC_clicked()
{

}

void FormDatHang::lamMoiNCC()
{
    cbNCCModel->setQuery(ddh->truyVanNCC);
}

void FormDatHang::lamMoiNSX()
{
    cbNSXModel->setQuery(ddh->truyVanNSX);
}

void FormDatHang::on_toolButtonThemNSX_clicked()
{

}

void FormDatHang::on_pushButtonTaoDDH_clicked()
{
    /*
     * Duyet danh sach trong bang lay du lieu tu moi dong sau do du no thanh kieu String JSON
    */
    QSharedPointer<DDH> ddhD(new DDH());
    ddhD.data()->noiDung = &modelDanhSachDuocChon;
    ddh->themDDH(ddhD.data());
    modelDanhSachDuocChon.clear();
}
