#include "formgia.h"
#include "ui_formgia.h"

FormGia::FormGia(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::FormGia)
{
    ui->setupUi(this);
    gia = new Gia();
    giaModel  = new QSqlQueryModel();
    ui->tableGia->setModel(giaModel);
    lamMoi();
    setWindowTitle("Quản lý giá ["+QString::fromLatin1(metaObject()->className())+"]");
    ui->dateEditNgayApDung->setDate(QDate::currentDate());
}

FormGia::~FormGia()
{
    delete ui;
    delete gia;
    delete giaModel;
}

void FormGia::on_tableGia_clicked(const QModelIndex &index)
{
    /*lay thong tin thuoc tu treeview dua vao lineedit*/
    ui->lineEditID->setText(giaModel->index(index.row(),0).data().toString());
    ui->lineEditTen->setText(giaModel->index(index.row(),1).data().toString());
}

void FormGia::on_buttonBox_accepted()
{
    /*them gia moi vao bang_gia voi ma thuoc*/
    QSharedPointer<OGia> ogia(new OGia());
    ogia.data()->ten = ui->lineEditTen->text();
    ogia.data()->id = ui->lineEditID->text();
    ogia.data()->gia = QString::number(ui->doubleSpinBoxGia->value(),'g',10);
    ogia.data()->ngayApDung = ui->dateEditNgayApDung->date().toString("yyyy-MM-dd");
    if(gia->themGia(ogia.data())){
        lamMoi();
        ui->doubleSpinBoxGia->setValue(0);
        ui->lineEditID->setText("");
        ui->lineEditTen->setText("");
    }
}

void FormGia::lamMoi()
{
    /*
                    * TODO: Lam phan nhap thuoc truoc khi lam gia
                    */
    giaModel->setQuery("SELECT HANG_HOA.HANGHOAID,HANG_HOA.TEN_HANG_HOA FROM HANG_HOA");
    giaModel->setHeaderData(1,Qt::Horizontal,QString::fromUtf8("Tên Hang"));
    giaModel->setHeaderData(0,Qt::Horizontal,"Mã");
}
