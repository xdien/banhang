#ifndef BCTONKHO_H
#define BCTONKHO_H

#include <QDialog>

#include "lrreportengine.h"
#include "lrcallbackdatasourceintf.h"
#include "chucnang/chitiet/ctmottonkho.h"
#include <QSqlQueryModel>
#include <QDebug>

namespace Ui {
class BCTonKho;
}

class BCTonKho : public QDialog
{
    Q_OBJECT

public:
    explicit BCTonKho(QWidget *parent = 0);
    ~BCTonKho();

private slots:
    void on_pushButtonThem_clicked();

    void on_treeViewTonKho_doubleClicked(const QModelIndex &index);

private:
    Ui::BCTonKho *ui;
    QSqlQueryModel *hangHoaModel;
    QSqlQuery query;
};

#endif // BCTONKHO_H
