#include "formtaikhoan.h"
#include "ui_formtaikhoan.h"

FormTaiKhoan::FormTaiKhoan(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::FormTaiKhoan)
{
    ui->setupUi(this);
    taiKhoan = new TaiKhoan();
    taiKhoanTableModel = new QSqlTableModel();
    ui->tableViewTaiKhoan->setModel(taiKhoanTableModel);
    taiKhoanTableModel->setTable("TAI_KHOAN");

    taiKhoanTableModel->select();
    taiKhoanTableModel->setEditStrategy(QSqlTableModel::OnFieldChange);
    setWindowTitle("Tài khoản ["+QString::fromLatin1(metaObject()->className())+"]");
    ui->tableViewTaiKhoan->hideColumn(1);
}

FormTaiKhoan::~FormTaiKhoan()
{
    delete ui;
    delete taiKhoanTableModel;
    delete taiKhoan;

}

void FormTaiKhoan::on_pushButtonThemTaiKHoan_clicked()
{
    QSharedPointer<OTaiKhoan> otaiKhoan(new OTaiKhoan());
    otaiKhoan.data()->username = ui->lineEditUseName->text();
    otaiKhoan.data()->pass = ui->lineEditPass->text();
    otaiKhoan.data()->diaChi = ui->plainTextEditDiaChi->document()->toPlainText();
    otaiKhoan.data()->tenNV = ui->lineEditHoTen->text();
    otaiKhoan.data()->setGioiTinh(ui->checkBoxGioiTinh->isChecked());
    if(taiKhoan->themTaiKhoan(otaiKhoan.data())){
        taiKhoanTableModel->select();
    }
}

void FormTaiKhoan::on_tableViewTaiKhoan_doubleClicked(const QModelIndex &index)
{
    /*hien thi formPhanQuyen*/
    FormPhanQuyen *phanQuyen = new FormPhanQuyen(taiKhoanTableModel->index(index.row(),0).data().toString());
    phanQuyen->show();
}
