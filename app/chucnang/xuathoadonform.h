#ifndef XUATHOADONFORM_H
#define XUATHOADONFORM_H

#include <QWidget>
#include <QDebug>
#include <QSqlQuery>
#include <QSqlQueryModel>
#include <QStandardItemModel>
#include "utils/xuathoadon.h"
#include "config/common.h"
#include <QMessageBox>
#include <QInputDialog>
#include <QMenu>
#include <QCompleter>
#include "Objects/xuathangmodel.h"

#include "utils/manageindex.h"
#define SELECT_STATEMENT_KHACH_HANG "select MST,TEN_KH,TEN_DN,DIA_CHI_KH,KHACHHANGID from KHACH_HANG "
#define SELECT_STATEMENT_HANG_HOA "select TEN_HANG_HOA,DVTID,HANGHOAID,SO_DU,GIA_BAN from HANG_HOA "

namespace Ui {
class XuatHoaDonForm;
}

class XuatHoaDonForm : public QWidget
{
    Q_OBJECT

public:
    explicit XuatHoaDonForm(QWidget *parent = 0);
    ~XuatHoaDonForm();

private slots:
    void on_tableViewHangHoa_doubleClicked(const QModelIndex &index);
    void xemThayDoi(QModelIndex index, QModelIndex value, QVector<int> ww);
void tinhTongTienHangChuaTT();
void on_doubleSpinBoxThue_valueChanged(double arg1);
void on_tableViewKhachHang_doubleClicked(const QModelIndex &index);

void on_pushButton_2_clicked();
void customMenuRequested(QPoint pos);
void customMenuHHDCRequested(QPoint pos);
void suaTonKho();

void on_pushButtonLamMoi_clicked();

void on_doubleSpinBoxTongTienHang_valueChanged(double arg1);

void on_doubleSpinBoxTienTHue_valueChanged(double arg1);

void on_doubleSpinBoxTongTien_valueChanged(double arg1);

void on_lineEdit_tenKhachHang_textChanged(const QString &arg1);

void on_lineEdit_tenKhachHang_returnPressed();
void autoCompleted(QModelIndex index);

void on_lineEditTenHangHoa_textChanged(const QString &arg1);
void chinhGiaBan();
void xoaHangHoaDuocChon();
void setHeaderForHHDC(QModelIndex index,int x ,int y);

private:
    Ui::XuatHoaDonForm *ui;
    XuatHoaDon *xuatHoaDon;

    QSqlQueryModel *khachHangModel;
    QSqlQueryModel *hangHoaModel;
    QStandardItemModel *hangHoaXuatModel;
    QList<QStandardItem *> append(const QString hangHoaID,
                                              const QString ten, const QString dvt,
                                                 const QString soLuong,
                                                 const QString donGia,
                                                 const QString tongTien);
    QSqlQuery query;
    double tongTienHang;
    void reset();
    QMenu *contextMenu;
    QAction *action_ChinhTonKho;
    QAction *action_ChinhGiaBan;
    QCompleter *mstComplete;
    QCompleter *tenDnComplete;
    QMenu *contextMenuHangHoaDC;//danh cho bang hang hoa duoc chon
    QAction *action_xoaHHDC;//xoa hang hoa duoc chon
};

#endif // XUATHOADONFORM_H
