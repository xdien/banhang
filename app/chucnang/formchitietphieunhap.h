#ifndef FORMCHITIETPHIEUNHAP_H
#define FORMCHITIETPHIEUNHAP_H

#include <QWidget>
#include <QStandardItemModel>
#include "utils/nhaphang.h"
#include "utils/chitietphieunhap.h"
#include "Objects/ophieuchi.h"
#include "Objects/ophieunhap.h"
#include "utils/thuoc.h"
#include "Objects/othuoc.h"

#define FORMAT_DATE "yyyy-MM-dd"

namespace Ui {
class FormChiTietPhieuNhap;
}

class FormChiTietPhieuNhap : public QWidget
{
    Q_OBJECT

public:
    explicit FormChiTietPhieuNhap(QString datHangId, QWidget *parent = 0);
    ~FormChiTietPhieuNhap();

private slots:
    void on_pushButton_3_clicked();

    void on_pushButton_clicked();

    void on_treeViewDatHang_clicked(const QModelIndex &index);

    void on_lineEditSolo_textChanged(const QString &arg1);

private:
    Ui::FormChiTietPhieuNhap *ui;
    QStandardItemModel *danhSachDat;
    QStandardItemModel *danhSachhangHienTai;
    QString donDatHangID;
    ChiTietPhieuNhap *chiTiet;
    NhapHang *nhapHang;
    Thuoc *thuoc;
    QList<QStandardItem *> add(const QString &tenThuoc,
                               const QString &soLo,
                               const QString &gia,
                               const QString &soLuong,
                               const QString &ngaySX,
                               const QString &ngayHetHan,
                               const QString &ncc,
                               const QString &nsx);

};

#endif // FORMCHITIETPHIEUNHAP_H
