#include "formhanghoa.h"
#include "ui_formhanghoa.h"

FormHangHoa::FormHangHoa(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::FormHangHoa)
{
    ui->setupUi(this);
    donvitinhModel = new QSqlQueryModel();
    hangHoaModel = new QSqlTableModel();
    nhomHangHoaModel = new QSqlTableModel();
    donViTinh = new DonViTinh();
    contextMenu = new QMenu(this);
    action_Xoa = new QAction("Xoa");
    completer = new QCompleter(this);
    donvitinhModel->setQuery("select DVTID from DON_VI_TINH");
    completer->setModel(donvitinhModel);
    ui->lineEditDonVi->setCompleter(completer);

    hangHoaModel->setTable("HANG_HOA");
    ui->tableView->setModel(hangHoaModel);
    hangHoaModel->select();
    hangHoaModel->setEditStrategy(QSqlTableModel::OnManualSubmit);
    //nhom hang hoa
    nhomHangHoaModel->setTable("NHOM_HANG_HOA");
    nhomHangHoaModel->select();
    ui->comboBox->setModel(nhomHangHoaModel);
    //khoi dong doi tuong hang hoa
    hangHoa = new HangHoa();
    contextMenu->addAction(action_Xoa);
    ui->tableView->verticalHeader()->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(ui->tableView->verticalHeader(),SIGNAL(customContextMenuRequested(QPoint)),this,SLOT(customMenuRequested(QPoint)));
    connect(action_Xoa,SIGNAL(triggered(bool)),this,SLOT(xoaHang()));
    ui->tableView->horizontalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
    hangHoaModel->setHeaderData(0,Qt::Horizontal,"Mã hàng");
    hangHoaModel->setHeaderData(1,Qt::Horizontal,"ĐVT");
    hangHoaModel->setHeaderData(2,Qt::Horizontal,"Nhóm hàng");
    hangHoaModel->setHeaderData(3,Qt::Horizontal,"Tên");
    hangHoaModel->setHeaderData(4,Qt::Horizontal,"Diễn giải");
    hangHoaModel->setHeaderData(5,Qt::Horizontal,"Tồn");
    hangHoaModel->setHeaderData(6,Qt::Horizontal,"Giá nhập");
    hangHoaModel->setHeaderData(7,Qt::Horizontal,"Giá bán");

}

FormHangHoa::~FormHangHoa()
{
    delete ui;
    delete donvitinhModel;
    delete completer;
    delete hangHoaModel;
    delete nhomHangHoaModel;
    delete hangHoa;
    delete donViTinh;
    delete contextMenu;
    delete action_Xoa;
}

void FormHangHoa::on_pushButton_clicked()
{
    /*Kiem tra index completer neu row or col = -1 la khong ton tai*/
    QString strDonVi = ui->lineEditDonVi->text();
    if(!donViTinh->kiemTraDonVi(strDonVi)){
        //Chua ton tai
        QMessageBox::StandardButton hoi;
        hoi = QMessageBox::question(this, "Chua ton tai", "Ban co muon them moi khong",
                                    QMessageBox::Yes | QMessageBox::No);
        if(hoi == QMessageBox::Yes){
            donViTinh->themDonVi(ui->lineEditDonVi->text());
        }

    }
    QSharedPointer<ManageIndex> genIndex(new ManageIndex());
    QString strindex =genIndex.data()->getNextIndexCode("HANG_HOA", "HH");
    //Thay doi MA HANG HOA cho phu hop voi chuong trinh cu
    if(hangHoa->themMoi(OHangHoa(strindex, ui->lineEditDonVi->text(),
                                 ui->comboBox->currentText(), ui->lineEditTenHang->text(),
                                 ui->lineEditDienGiai->text())))
        hangHoaModel->select();
}

void FormHangHoa::on_toolButton_clicked()
{
    //goi form nhom hang hoa
    FormNhomHangHoa *nhomHH;
    nhomHH = new FormNhomHangHoa();
    nhomHH->setAttribute(Qt::WA_DeleteOnClose,true);
    connect(nhomHH,SIGNAL(sukienThem()),nhomHangHoaModel,SLOT(select()));
    nhomHH->show();

}
void FormHangHoa::customMenuRequested(QPoint pos)
{
    contextMenu->popup(ui->tableView->viewport()->mapToGlobal(pos));
    ui->tableView->setCurrentIndex(ui->tableView->indexAt(pos));
}

void FormHangHoa::xoaHang()
{
    if(hangHoa->xoaHang(hangHoaModel->index(ui->tableView->currentIndex().row(),0).data().toString())){
        hangHoaModel->select();
    }
}

void FormHangHoa::on_pushButtonLuu_clicked()
{
    hangHoaModel->submit();
}

void FormHangHoa::on_toolButtonimportExcel_clicked()
{

}
