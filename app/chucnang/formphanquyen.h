#ifndef FORMPHANQUYEN_H
#define FORMPHANQUYEN_H

#include <QWidget>
#include <QSqlQueryModel>
#include <QStandardItemModel>
#include <QSqlQuery>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>

namespace Ui {
class FormPhanQuyen;
}

class FormPhanQuyen : public QWidget
{
    Q_OBJECT

public:
    explicit FormPhanQuyen(QString username,QWidget *parent = 0);
    ~FormPhanQuyen();
    QString username;

private slots:
    void on_treeViewQuyenuyen_clicked(const QModelIndex &index);

    void on_pushButton_clicked();

private:
    Ui::FormPhanQuyen *ui;
    QSqlQueryModel *quyenModel;
    QStandardItemModel *quyenChonModel;
    QList<QStandardItem *> add(const QString &formid);
    QSqlQuery *query;
};

#endif // FORMPHANQUYEN_H
