#include "formduyetkh.h"
#include "ui_formduyetkh.h"

FormDuyetKH::FormDuyetKH(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::FormDuyetKH)
{
    ui->setupUi(this);
    dskhachhang = new QSqlQueryModel();
    ui->tableView->setModel(dskhachhang);
    dskhachhang->setQuery("select KHACHHANGID,TEN_KH,TEN_DN,DIA_CHI_KH,SO_DIEN_THOAI,MAU_SO,KY_HIEU from KHACH_HANG limit 50");
    dskhachhang->setHeaderData(0,Qt::Horizontal,"Mã KH");
    dskhachhang->setHeaderData(1,Qt::Horizontal,"Tên");
    dskhachhang->setHeaderData(2,Qt::Horizontal,"Tên đơn vị");
    dskhachhang->setHeaderData(3,Qt::Horizontal,"Địa chỉ");
    dskhachhang->setHeaderData(4,Qt::Horizontal,"SĐT");
    dskhachhang->setHeaderData(5,Qt::Horizontal,"Mẫu số HĐ");
    dskhachhang->setHeaderData(6,Qt::Horizontal,"Ký hiệu HĐ");

}

FormDuyetKH::~FormDuyetKH()
{
    delete ui;
    delete dskhachhang;
    qDebug() << "giai phong bo nho FormDuyetKH";
}

void FormDuyetKH::on_pushButton_clicked()
{

    QSharedPointer<OKhachHang> khachHang(new OKhachHang());
    khachHang.data()->DIA_CHI_KH = dskhachhang->index(ui->tableView->currentIndex().row(),3).data().toString();
    khachHang.data()->KHACHHANGID = dskhachhang->index(ui->tableView->currentIndex().row(),0).data().toString();
    khachHang.data()->TEN_KHACH_HANG = dskhachhang->index(ui->tableView->currentIndex().row(),1).data().toString();
    khachHang.data()->KyHieuHD = dskhachhang->index(ui->tableView->currentIndex().row(),6).data().toString();
    khachHang.data()->tenDonVi = dskhachhang->index(ui->tableView->currentIndex().row(),2).data().toString();
    khachHang.data()->sdt = dskhachhang->index(ui->tableView->currentIndex().row(),4).data().toString();
    khachHang.data()->mauSoHD = dskhachhang->index(ui->tableView->currentIndex().row(),5).data().toString();
    emit guiThongTinKH(khachHang.data());
    this->close();
}

void FormDuyetKH::on_pushButton_2_clicked()
{
    dskhachhang->setQuery("select KHACHHANGID,TEN_KH,DIA_CHI_KH"
                          " from KHACH_HANG where TEN_KH like '%"+ui->lineEdit->text()+"%' limit 10");
}

void FormDuyetKH::on_toolButtonThemKH_clicked()
{
    QScopedPointer<DialogKhachHang> fmoi;
    fmoi.reset(new DialogKhachHang());
    fmoi->exec();
}
