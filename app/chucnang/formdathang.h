#ifndef FORMDATHANG_H
#define FORMDATHANG_H

#include <QWidget>
#include <QSqlQueryModel>
#include <QStandardItemModel>
#include <QModelIndex>
#include "utils/dondathang.h"
#include <QDebug>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonDocument>

namespace Ui {
class FormDatHang;
}

class FormDatHang : public QWidget
{
    Q_OBJECT

public:
    explicit FormDatHang(QWidget *parent = 0);
    ~FormDatHang();

private slots:
    void on_treeViewThuoc_clicked(const QModelIndex &index);

    void on_pushButtonThem_clicked();

    void on_toolButtonThemNCC_clicked();
    void lamMoiNCC();
    void lamMoiNSX();

    void on_toolButtonThemNSX_clicked();

    void on_pushButtonTaoDDH_clicked();

private:
    Ui::FormDatHang *ui;
    QSqlQueryModel *modelDanhSachGoiY;
    QStandardItemModel modelDanhSachDuocChon;
    QSqlQueryModel *cbNCCModel,*cbNSXModel;
    DonDatHang *ddh;
    //ham dung de cau truc du lieu thanh mot QStandardItem de append vao modelDanhSachDuocChon
    QList<QStandardItem *> dongKhungThuoc(const QString &tenThuoc,
                                                            const QString &tenNCC,
                                                            const QString &tenNSX,
                                                            const QString &idNCC,
                                                            const QString &idNSX,
                                                            const QString &soLuong);

};

#endif // FORMDATHANG_H
