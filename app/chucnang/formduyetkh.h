#ifndef FORMDUYETKH_H
#define FORMDUYETKH_H

#include <QWidget>
#include "Objects/okhachhang.h"
#include <QSqlQueryModel>
#include <QDebug>
#include "dialogkhachhang.h"

namespace Ui {
class FormDuyetKH;
}

class FormDuyetKH : public QWidget
{
    Q_OBJECT

public:
    explicit FormDuyetKH(QWidget *parent = 0);
    ~FormDuyetKH();
signals:
    void guiThongTinKH(OKhachHang *okhachHang);
private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

    void on_toolButtonThemKH_clicked();

private:
    Ui::FormDuyetKH *ui;
    QSqlQueryModel *dskhachhang;
};

#endif // FORMDUYETKH_H
