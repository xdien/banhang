#include "formphieunhap.h"
#include "ui_formphieunhap.h"

#include <QQmlEngine>
#include <QQuickItem>

FormPhieuNhap::FormPhieuNhap(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::FormPhieuNhap)
{
    ui->setupUi(this);
    tableModel = new QSqlQueryModel();
    phieuChi = new PhieuChi();
    ui->listViewDatHang->setModel(tableModel);
    tableModel->setQuery("select DONDATHANGID,NGAY_LAP_DDH from DON_DAT_HANG");
    setWindowTitle("Phiếu nhập ["+QString::fromLatin1(metaObject()->className())+"]");
}

FormPhieuNhap::~FormPhieuNhap()
{
    delete ui;
    delete tableModel;
    delete phieuChi;
}

void FormPhieuNhap::on_listViewDatHang_clicked(const QModelIndex &index)
{
    FormChiTietPhieuNhap *fctNhap = new FormChiTietPhieuNhap(tableModel->index(index.row(),0).data().toString());
    fctNhap->show();
}

void FormPhieuNhap::on_pushButton_clicked()
{
    tableModel->setQuery("select DONDATHANGID,NGAY_LAP_DDH from DON_DAT_HANG where DONDATHANGID like '%"+ui->lineEditTimKiem->text()+"%'");
}
