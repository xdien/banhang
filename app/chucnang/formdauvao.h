#ifndef FORMDAUVAO_H
#define FORMDAUVAO_H

#include <QWidget>
#include "utils/hoadonvao.h"
#include "utils/chitiethoadonvao.h"
#include <QStandardItem>
#include "utils/manageindex.h"
#include <QCompleter>
#include "utils/hanghoa.h"
#include "utils/donvitinh.h"
#include "utils/taikhoan.h"
#include <QMenu>
#include "chucnang/formduyetkh.h"
#include <qmath.h>
#include <QStandardItem>

#include "Objects/okhachhang.h"
namespace Ui {
class FormDauVao;
}

class FormDauVao : public QWidget
{
    Q_OBJECT

public:
    explicit FormDauVao(QWidget *parent = 0);
    ~FormDauVao();

private slots:
    void on_pushButtonHoanThanh_clicked();

    void on_pushButtonThem_clicked();

private:
    QList<QStandardItem *> appendDanhSachHangHoa(const QString &maHH,
                                                 const QString &tenHH,
                                                 const QString &dvt,
                                                 const double soLuong,
                                                 const double donGia,
                                                 const double tongTien);
    QStandardItemModel *danhSachModel;
    Ui::FormDauVao *ui;
    HoaDonVao *hoaDonVao;
    QSqlQueryModel *dshangModel;
    QCompleter *completerID,*completerTenHang, *completerKhachHang;
    DonViTinh *donViTinh;
    HangHoa *hangHoa;
    TaiKhoan taiKhoan;
    double tongSoTien;
    QString m_dvtid;
    double tongTienHang;
    QMenu *contextMenu;
    QAction *action_xoa;
private slots:
    void customMenuRequested(QPoint pos);
    void on_lineEditTenHang_editingFinished();
    void nhanTTKhachHang(OKhachHang *okhachHang);
    void on_toolButton_clicked();
    void autoCompletedID(QModelIndex index);
    void autoCompletedTenHang(QModelIndex index);

    void on_tongTienHang_valueChanged(double arg1);
    void on_VAT_valueChanged(double arg1);
    void on_tienThue_valueChanged(double arg1);
    void tinhTongTienHangChuaTT();
    void xoaHangHoaTrongDanhSachModel();
};

#endif // FORMDAUVAO_H
