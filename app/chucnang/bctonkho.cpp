#include "bctonkho.h"
#include "ui_bctonkho.h"

BCTonKho::BCTonKho(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::BCTonKho)
{
    ui->setupUi(this);

    hangHoaModel = new QSqlQueryModel();
    hangHoaModel->setQuery("select HANGHOAID,TEN_HANG_HOA,SO_DU from hang_hoa");
    ui->treeViewTonKho->setModel(hangHoaModel);
    ui->dateEditDen->setDate(QDate::currentDate());
}

BCTonKho::~BCTonKho()
{
    delete ui;
   delete hangHoaModel;
}

void BCTonKho::on_pushButtonThem_clicked()
{
    /*LimeReport::ReportEngine *report = new LimeReport::ReportEngine(this);
      //report->dataManager()->addModel("string_list",stringListModel,true);
      report->loadFromFile(QApplication::applicationDirPath()+"/reports/baocaotonkho.lrxml");
      qDebug()<< report->lastError();
      //m_preview->refreshPages();
      report->previewReport();*/
    query.exec("select id,COLUMN_JSON(info) from tavble");
    qDebug() << query.lastError().text();
    while (query.next()) {
        qDebug() << query.value(1).toString();
    }
}

void BCTonKho::on_treeViewTonKho_doubleClicked(const QModelIndex &index)
{
    CTMotTonKho *chiTiet = new CTMotTonKho(hangHoaModel->index(index.row(),0).data().toString(),ui->dateEditTu->date(),ui->dateEditDen->date());
    chiTiet->setAttribute(Qt::WA_DeleteOnClose,true);
    chiTiet->show();
}
