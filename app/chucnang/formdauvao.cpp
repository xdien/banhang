#include "formdauvao.h"
#include "ui_formdauvao.h"

FormDauVao::FormDauVao(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::FormDauVao)
{
    ui->setupUi(this);
    hoaDonVao = new HoaDonVao();
    danhSachModel = new QStandardItemModel();
    ui->tableViewDSHang->setModel(danhSachModel);
    dshangModel = new QSqlQueryModel();
    completerID = new QCompleter(this);
    completerTenHang = new QCompleter(this);
    completerKhachHang = new QCompleter(this);
    contextMenu = new QMenu(this);
    action_xoa = new QAction("Xóa");
    contextMenu->addAction(action_xoa);
    dshangModel->setQuery("select TEN_HANG_HOA,DVTID,HANGHOAID,SO_DU,GIA_BAN from HANG_HOA");
    completerID->setModel(dshangModel);
    completerTenHang->setModel(dshangModel);
    completerID->setCompletionColumn(2);
    completerTenHang->setCompletionColumn(0);
    ui->lineEditTenHang->setCompleter(completerTenHang);
    ui->lineEditMaHang->setCompleter(completerID);
    donViTinh = new DonViTinh();
    hangHoa = new HangHoa();
    ui->tableViewDSHang->verticalHeader()->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(ui->tableViewDSHang->verticalHeader(),SIGNAL(customContextMenuRequested(QPoint)),
            this,SLOT(customMenuRequested(QPoint)));
    tongSoTien = 0;
    connect(completerID,SIGNAL(activated(QModelIndex)),this,SLOT(autoCompletedID(QModelIndex)));
    connect(completerTenHang,SIGNAL(activated(QModelIndex)),this,SLOT(autoCompletedTenHang(QModelIndex)));
    connect(danhSachModel,SIGNAL(rowsInserted(QModelIndex,int,int)),this,SLOT(tinhTongTienHangChuaTT()));
    connect(danhSachModel,SIGNAL(rowsRemoved(QModelIndex,int,int)),this,SLOT(tinhTongTienHangChuaTT()));
    connect(action_xoa,SIGNAL(triggered(bool)),this,SLOT(xoaHangHoaTrongDanhSachModel()));

}

FormDauVao::~FormDauVao()
{
    delete ui;
    delete hoaDonVao;
    delete danhSachModel;
    delete dshangModel;
    delete completerID;
    delete completerTenHang;
    delete donViTinh;
    delete hangHoa;
    delete completerKhachHang;
    delete action_xoa;
    delete contextMenu;
}

void FormDauVao::on_pushButtonHoanThanh_clicked()
{
    ManageIndex genIndex;
    OHoaDonVao ohoaDonVao;
    ohoaDonVao.HOADONVAOID = genIndex.getNextIndexCode("HOA_DON_VAO","HV");
    ohoaDonVao.USERNAME = taiKhoan.layIDDangNhapHienTai();
    ohoaDonVao.tongTienCuoi = ui->tongTienCuoiCung->value();
    ohoaDonVao.tongTienHang = ui->tongTienHang->value();
    ohoaDonVao.vat = ui->VAT->value();
    ohoaDonVao.tienThue = ui->tienThue->value();
    ohoaDonVao.okhachHang.MST = ui->lineEditMST->text();
    ohoaDonVao.okhachHang.KHACHHANGID = ui->lineEditMaKhachHang->text();
    ohoaDonVao.okhachHang.tenDonVi = ui->lineEditTenDN->text();
    ohoaDonVao.okhachHang.DIA_CHI_KH = ui->lineEditDiaChi->text();
    ohoaDonVao.KY_HIEU = ui->lineEditKyHieuHD->text();
    ohoaDonVao.MAU_SO = ui->lineEditMauSoHD->text();
    ohoaDonVao.SO_HIEU = ui->lineEditSoHD->text();
    ohoaDonVao.NGAY_LAP_HDV = ui->dateEditNgayHD->date();
    QString strindex = hoaDonVao->taoMoi(ohoaDonVao);
    if(strindex != NULL){
        ChiTietHoaDonVao *chiTietHoaDonvao = new ChiTietHoaDonVao(strindex);
        /*chay vong lap for cho bang danh sach hang hoa transaction cho mysql*/
        for (int i = 0; i < danhSachModel->rowCount(); ++i) {
            chiTietHoaDonvao->append(OChiTietHoaDonvao(danhSachModel->index(i,0).data().toString(),
                                                       danhSachModel->index(i,2).data().toString(),
                                                       danhSachModel->index(i,3).data().toString(),
                                                       danhSachModel->index(i,4).data().toString()));

        }
        tongSoTien = 0;
        //reset
    }
}

void FormDauVao::on_pushButtonThem_clicked()
{
    //tinh don gia neu so luong = 0 thi khong the tinh
    if(ui->itemSoLuongNhap->value() > 0){
        if(danhSachModel->findItems(ui->lineEditMaHang->text(), Qt::MatchExactly,0).count() <= 0){
            danhSachModel->appendRow(appendDanhSachHangHoa(ui->lineEditMaHang->text(),
                                                           ui->lineEditTenHang->text(),
                                                           ui->lineEditDVT->text(),
                                                           ui->itemSoLuongNhap->value(),
                                                           ui->itemGiaNhap->value(),
                                                           ui->itemTongSoTien->value()));
            tongSoTien = ui->itemTongSoTien->value()+ui->tongTienCuoiCung->value();
            //ui->lineEditTongSoTien->setText(QString("%1%2").arg(tongSoTien < 0 ? '-' : '+').arg(qFabs(tongSoTien),5,'f',2,'0'));
            ui->lineEditMaHang->setText("");
            ui->lineEditDVT->setText("");
            ui->lineEditTenHang->setText("");
            ui->itemGiaNhap->setValue(0);
        }
    }
}

QList<QStandardItem *> FormDauVao::appendDanhSachHangHoa(const QString &maHH,
                                                        const QString &tenHH,
                                                         const QString &dvt,
                                                        const double soLuong,
                                                        const double donGia,
                                                         const double tongTien)
{
    QList<QStandardItem *> rowItems;
    QStandardItem *donGiaItem = new QStandardItem();
    donGiaItem->setData(donGia, Qt::EditRole);
    QStandardItem *tongTienItem = new QStandardItem();
    tongTienItem->setData(tongTien, Qt::EditRole);
    QStandardItem *soLuongItem = new QStandardItem();
    soLuongItem->setData(soLuong, Qt::EditRole);
    rowItems << new QStandardItem(maHH);
    rowItems << new QStandardItem(tenHH);
    rowItems << new QStandardItem(dvt);
    rowItems << soLuongItem;
    rowItems << donGiaItem;
    rowItems << tongTienItem;
    return rowItems;
}

void FormDauVao::customMenuRequested(QPoint pos)
{
    QModelIndex index=ui->tableViewDSHang->indexAt(pos);
    ui->tableViewDSHang->selectRow(index.row());
    contextMenu->popup(ui->tableViewDSHang->viewport()->mapToGlobal(pos));
}

void FormDauVao::on_lineEditTenHang_editingFinished()
{
    ui->lineEditMaHang->setText( hangHoa->kiemTraTenHang(ui->lineEditTenHang->text()));
}

void FormDauVao::nhanTTKhachHang(OKhachHang *okhachHang)
{
    ui->lineEditMST->setText(okhachHang->KHACHHANGID);
    ui->lineEditDiaChi->setText(okhachHang->DIA_CHI_KH);
    ui->lineEditTenDN->setText(okhachHang->TEN_KHACH_HANG);
    //ui->lin
    ui->lineEditKyHieuHD->setText(okhachHang->KyHieuHD);
    ui->lineEditMauSoHD->setText(okhachHang->mauSoHD);
    ui->lineEditSTD->setText(okhachHang->sdt);
    ui->lineEditTenDN->setText(okhachHang->tenDonVi);
}

void FormDauVao::on_toolButton_clicked()
{
    FormDuyetKH *duyet = new FormDuyetKH();
    connect(duyet,SIGNAL(guiThongTinKH(OKhachHang*)),this,SLOT(nhanTTKhachHang(OKhachHang*)));
    duyet->show();
}
void FormDauVao::autoCompletedID(QModelIndex index)
{
    ui->lineEditDVT->setText(dshangModel->index(index.row(),1).data().toString());
    ui->lineEditTenHang->setText(dshangModel->index(index.row(),0).data().toString());
    ui->itemGiaNhap->setValue(dshangModel->index(index.row(),4).data().toFloat(0));
}
void FormDauVao::autoCompletedTenHang(QModelIndex index)
{
    ui->lineEditDVT->setText(dshangModel->index(index.row(),1).data().toString());
    ui->lineEditMaHang->setText(dshangModel->index(index.row(),2).data().toString());
    ui->itemGiaNhap->setValue(dshangModel->index(index.row(),4).data().toFloat(0));
}

void FormDauVao::on_tongTienHang_valueChanged(double arg1)
{
    //tinh tien thue
    ui->tienThue->setValue(round(arg1*ui->VAT->value()/100));
}

void FormDauVao::on_VAT_valueChanged(double arg1)
{
    ui->tienThue->setValue(round(arg1*ui->tongTienHang->value()/100));
}

void FormDauVao::on_tienThue_valueChanged(double arg1)
{
    //set tong tien cuoi cung
    ui->tongTienCuoiCung->setValue(round(ui->tongTienHang->value()+arg1));
}

void FormDauVao::tinhTongTienHangChuaTT()
{
    tongTienHang = 0;
    //chay vong lap for cho den het danh sach hang
    for (int i = 0; i < danhSachModel->rowCount(); ++i) {
            tongTienHang += danhSachModel->index(i,5).data().toDouble();
    }
    ui->tongTienHang->setValue(tongTienHang);
    //ui->spinBoxSoTienDaTra->setValue(soTienDaTra);
}

void FormDauVao::xoaHangHoaTrongDanhSachModel()
{
    if(danhSachModel->removeRow(ui->tableViewDSHang->currentIndex().row())){

        //hangHoaModel->setQuery(hangHoaModel->query().lastQuery());

    }
}
