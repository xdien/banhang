#include "formphanquyen.h"
#include "ui_formphanquyen.h"

FormPhanQuyen::FormPhanQuyen(QString username, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::FormPhanQuyen)
{
    ui->setupUi(this);
    quyenModel = new QSqlQueryModel();
    quyenChonModel = new QStandardItemModel();
    ui->treeViewCoQuyenoQuyen->setModel(quyenChonModel);
    ui->treeViewQuyenuyen->setModel(quyenModel);
    this->username = username;
    query = new QSqlQuery();
    quyenModel->setQuery("select * from QUYEN");
    /*Lay thong tin quyen cua nhan vien hien tai*/
    query->exec("select coquyen from TAI_KHOAN where username ='"+this->username+"'");
    if(query->next()){
        QJsonDocument doc = QJsonDocument::fromJson(query->value(0).toString().toUtf8());
        QJsonArray ar = doc.array();
        for (int i = 0; i < ar.count(); ++i) {
            quyenChonModel->appendRow(add(ar[i].toObject()["formid"].toString()));
        }
    }
    ui->lineEditTenNV->setText(username);
    setWindowTitle("Phân quyền cho nhân viên ["+QString::fromLatin1(metaObject()->className())+"]");

}

FormPhanQuyen::~FormPhanQuyen()
{
    delete ui;
    delete quyenChonModel;
    delete quyenModel;
    delete query;
}
QList<QStandardItem *> FormPhanQuyen::add(const QString &formid)
{
    QList<QStandardItem *> rowItems;
    rowItems << new QStandardItem(formid);
    return rowItems;
}

void FormPhanQuyen::on_treeViewQuyenuyen_clicked(const QModelIndex &index)
{
    if(quyenChonModel->findItems(quyenModel->index(index.row(),0).data().toString(),
                                 Qt::MatchExactly,0).length() == 0){
        quyenChonModel->appendRow(add(quyenModel->index(index.row(),0).data().toString()));
    }

}

void FormPhanQuyen::on_pushButton_clicked()
{
    /*cap nhat vao nhan vien*/
    //duyet danh sach quyen
    QJsonArray ar;
    for (int i = 0; i < quyenChonModel->rowCount(); ++i) {
        QJsonObject dongDuLieuJSON;
        dongDuLieuJSON["formid"] = quyenChonModel->index(i,0).data().toString();
        ar.append(dongDuLieuJSON);
    }
    QJsonDocument doc(ar);
    query->exec("UPDATE TAI_KHOAN SET COQUYEN = '"+doc.toJson(QJsonDocument::Compact)+"' WHERE USERNAME = '"+username+"'");
}
