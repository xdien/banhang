#ifndef CTMOTTONKHO_H
#define CTMOTTONKHO_H

#include <QDialog>
#include "utils/hanghoa.h"
#include <QSqlQueryModel>
#include <QDate>

namespace Ui {
class CTMotTonKho;
}

class CTMotTonKho : public QDialog
{
    Q_OBJECT

public:
    explicit CTMotTonKho(const QString hhid,const QDate tu,const QDate den, QWidget *parent = 0);
    ~CTMotTonKho();

private:
    Ui::CTMotTonKho *ui;
    QString m_hangHoaId;
    HangHoa *hangHoa;
    QSqlQueryModel *chitietHangModel;
};

#endif // CTMOTTONKHO_H
