#include "ctmottonkho.h"
#include "ui_ctmottonkho.h"

CTMotTonKho::CTMotTonKho(const QString hhid, const QDate tu, const QDate den, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::CTMotTonKho),
    m_hangHoaId(hhid)
{
    ui->setupUi(this);
    hangHoa = new HangHoa();
    chitietHangModel = new QSqlQueryModel();
    chitietHangModel->setQuery("SELECT chi_tiet_vao.SO_LUONG as Vao,"
                               "null AS Ra,hoa_don_vao.THOI_GIAN_TAO "
                               "FROM hoa_don_vao RIGHT JOIN chi_tiet_vao ON "
                               "hoa_don_vao.HOADONVAOID = chi_tiet_vao.HOADONVAOID "
                               "where DATE(THOI_GIAN_TAO) > '"+tu.toString("yyyy-MM-dd")+"' "
                               "and DATE(THOI_GIAN_TAO) <= '"+den.toString("yyyy-MM-dd")+"' and "
                               "HANGHOAID = '"+m_hangHoaId+"' "
                               "UNION "
                               "SELECT null,chi_tiet_ra.SO_LUONG,hoa_don_ra.NGAY_LAP FROM "
                               "hoa_don_ra RIGHT JOIN chi_tiet_ra ON "
                               "hoa_don_ra.HOADONRAID = chi_tiet_ra.HOADONRAID "
                               "where DATE(NGAY_LAP) > '"+tu.toString("yyyy-MM-dd")+"' and "
                               "DATE(NGAY_LAP) <= '"+den.toString("yyyy-MM-dd")+"' "
                               "and HANGHOAID = '"+m_hangHoaId+"'");
    ui->treeViewChiTiet->setModel(chitietHangModel);
    ui->lineEditDen->setText(den.toString("dd/MM/yyyy"));
    ui->lineEditTu->setText(tu.toString("dd/MM/yyyy"));
    ui->labeltenhang->setText(hangHoa->layTenHang(m_hangHoaId));
}

CTMotTonKho::~CTMotTonKho()
{
    delete ui;
    delete hangHoa;
    delete chitietHangModel;
}
