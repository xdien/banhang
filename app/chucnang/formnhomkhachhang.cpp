#include "formnhomkhachhang.h"
#include "ui_formnhomkhachhang.h"

#include <QMenu>

FormNhomKhachHang::FormNhomKhachHang(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::FormNhomKhachHang)
{
    ui->setupUi(this);
    nkh = new NhomKhachHang(this);
    nkhModel = new QSqlTableModel();
    nkhModel->setEditStrategy(QSqlTableModel::OnRowChange);
    nkhModel->setTable("NHOM_KHACH_HANG");
    nkhModel->select();
    ui->tableNKH->setModel(nkhModel);
    ui->tableNKH->verticalHeader()->setContextMenuPolicy(Qt::CustomContextMenu);//right lick
    connect(ui->tableNKH->verticalHeader(),SIGNAL(customContextMenuRequested(QPoint)),this,SLOT(customMenuRequested(QPoint)));
    setWindowTitle("Nhóm khách hàng ["+QString::fromLatin1(metaObject()->className())+"]");
}

FormNhomKhachHang::~FormNhomKhachHang()
{
    delete ui;
    delete nkh;
    delete nkhModel;
}

void FormNhomKhachHang::on_buttonBox_accepted()
{
    QSharedPointer<NKH> Oncc(new NKH());
    Oncc.data()->moTa = ui->lineEditMoTa->text();
    Oncc.data()->chietKhau = ui->lineEditChietKhau->text();
    if(nkh->themNKH(Oncc.data())){
        nkhModel->select();//refesh
        emit lamMoi();
    }


}
void FormNhomKhachHang::customMenuRequested(QPoint pos)
{
    QModelIndex index=ui->tableNKH->indexAt(pos);
    ui->tableNKH->selectRow(index.row());
    QMenu *menu =new QMenu(this);
    menu->addAction(new QAction("Delete", this));
    menu->popup(ui->tableNKH->viewport()->mapToGlobal(pos));
    connect(menu,SIGNAL(triggered(QAction*)),this,SLOT(xoaNKH(QAction*)));
}

void FormNhomKhachHang::xoaNKH(QAction *id)
{
    if(id->text() == "Delete"){
        nkhModel->removeRow(ui->tableNKH->currentIndex().row());
        nkhModel->submit();
        nkhModel->select();
        emit lamMoi();
    }
}
