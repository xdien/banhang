#ifndef FORMPHIEUCHI_H
#define FORMPHIEUCHI_H

#include <QWidget>
#include <QSqlTableModel>
#include "utils/phieuchi.h"
#include "Objects/ophieuchi.h"
#include "utils/taikhoan.h"

namespace Ui {
class FormPhieuChi;
}

class FormPhieuChi : public QWidget
{
    Q_OBJECT

public:
    explicit FormPhieuChi(QWidget *parent = 0);
    ~FormPhieuChi();

private slots:
    void on_pushButton_clicked();

private:
    Ui::FormPhieuChi *ui;
    QSqlTableModel *tableModelPhieuChi;
    PhieuChi *phieuChi;
    TaiKhoan *taiKhoan;
};

#endif // FORMPHIEUCHI_H
