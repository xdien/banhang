#ifndef FORMGIA_H
#define FORMGIA_H

#include <QWidget>
#include "utils/gia.h"
#include "Objects/ogia.h"
#include <QSqlQueryModel>

namespace Ui {
class FormGia;
}

class FormGia : public QWidget
{
    Q_OBJECT

public:
    explicit FormGia(QWidget *parent = 0);
    ~FormGia();

private slots:
    void on_tableGia_clicked(const QModelIndex &index);

    void on_buttonBox_accepted();
    void lamMoi();
private:
    Ui::FormGia *ui;
    Gia *gia;
    QSqlQueryModel *giaModel;
};

#endif // FORMGIA_H
