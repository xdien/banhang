#include "formquyen.h"
#include "ui_formquyen.h"

FormQuyen::FormQuyen(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::FormQuyen)
{
    ui->setupUi(this);
    quyen = new Quyen();
    tableModelQuyen = new QSqlTableModel();
    tableModelQuyen->setTable("QUYEN");
    tableModelQuyen->select();
    ui->tableViewQuyen->setModel(tableModelQuyen);
    setWindowTitle("Quyền ["+QString::fromLatin1(metaObject()->className())+"]");
}

FormQuyen::~FormQuyen()
{
    delete ui;
    delete quyen;
    delete tableModelQuyen;
}

void FormQuyen::on_pushButton_2_clicked()
{
    QSharedPointer<OQuyen> oquyen(new OQuyen());
    oquyen.data()->formid = ui->lineEditFormID->text();
    oquyen.data()->moTa = ui->lineEditMoTa->text();
    if(quyen->themQuyen(oquyen.data())){
        tableModelQuyen->select();
    }
}
void FormQuyen::xoaQuyen(QAction *id)
{
    if(id->text() == "Delete"){
        /*ui->lineEditTongSoTien->setText(QString::number(ui->lineEditTongSoTien->text().toDouble(0)-
                                                        danhSachModel->index(ui->tableViewDSHang->currentIndex().row(),4).data().toDouble(0)));
        danhSachModel->removeRow(ui->tableViewDSHang->currentIndex().row());*/

    }
}
