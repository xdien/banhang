#ifndef FORMPHIEUNHAP_H
#define FORMPHIEUNHAP_H

#include <QWidget>
#include <QUrl>
#include <QSqlTableModel>
#include "utils/phieuchi.h"
#include "chucnang/formchitietphieunhap.h"
namespace Ui {
class FormPhieuNhap;
}

class FormPhieuNhap : public QWidget
{
    Q_OBJECT

public:
    explicit FormPhieuNhap(QWidget *parent = 0);
    ~FormPhieuNhap();

private slots:
    void on_listViewDatHang_clicked(const QModelIndex &index);

    void on_pushButton_clicked();

private:
    Ui::FormPhieuNhap *ui;
    QSqlQueryModel *tableModel;
    PhieuChi *phieuChi;
};

#endif // FORMPHIEUNHAP_H
