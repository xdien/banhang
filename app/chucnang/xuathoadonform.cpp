#include "xuathoadonform.h"
#include "ui_xuathoadonform.h"

XuatHoaDonForm::XuatHoaDonForm(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::XuatHoaDonForm)
{
    ui->setupUi(this);
    hangHoaModel = new QSqlQueryModel();
    khachHangModel = new QSqlQueryModel();
    hangHoaXuatModel = new QStandardItemModel();
    xuatHoaDon = new XuatHoaDon();
    contextMenu = new QMenu(this);
    contextMenuHangHoaDC = new QMenu(this);
    action_xoaHHDC = new QAction("Xóa");
    contextMenuHangHoaDC->addAction(action_xoaHHDC);
    mstComplete = new QCompleter();
    tenDnComplete = new QCompleter();
    action_ChinhTonKho = new QAction(this);
    action_ChinhGiaBan = new QAction(this);
    action_ChinhTonKho->setText("Chỉnh tồn");
    action_ChinhGiaBan->setText("Chỉnh giá bán");
    contextMenu->addAction(action_ChinhTonKho);
    contextMenu->addAction(action_ChinhGiaBan);
    ui->tableViewHangHoa->verticalHeader()->setContextMenuPolicy(Qt::CustomContextMenu);
    ui->tableView->verticalHeader()->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(ui->tableViewHangHoa->verticalHeader(),SIGNAL(customContextMenuRequested(QPoint)),this,SLOT(customMenuRequested(QPoint)));
    connect(ui->tableView->verticalHeader(),SIGNAL(customContextMenuRequested(QPoint)),this,SLOT(customMenuHHDCRequested(QPoint)));

    hangHoaModel->setQuery(tr(SELECT_STATEMENT_HANG_HOA));
    khachHangModel->setQuery(tr(SELECT_STATEMENT_KHACH_HANG));
    mstComplete->setModel(khachHangModel);
    tenDnComplete->setModel(khachHangModel);
    mstComplete->setCompletionColumn(0);
    tenDnComplete->setCompletionColumn(2);
    //mstComplete->setCompletionRole(1);
    ui->lineEditMST->setCompleter(mstComplete);
    ui->lineEditTenDonVi->setCompleter(tenDnComplete);
    ui->tableView->setModel(hangHoaXuatModel);
    ui->tableViewHangHoa->setModel(hangHoaModel);
    ui->tableViewKhachHang->setModel(khachHangModel);
    ui->tableViewKhachHang->horizontalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
    ui->tableView->horizontalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
    ui->tableViewHangHoa->horizontalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
    connect(hangHoaXuatModel,SIGNAL(dataChanged(QModelIndex,QModelIndex,QVector<int>)),this,SLOT(tinhTongTienHangChuaTT()));
    connect(hangHoaXuatModel,SIGNAL(rowsRemoved(QModelIndex,int,int)),this,SLOT(tinhTongTienHangChuaTT()));
    connect(hangHoaXuatModel,SIGNAL(rowsInserted(QModelIndex,int,int)),this,SLOT(tinhTongTienHangChuaTT()));
    connect(hangHoaXuatModel,SIGNAL(rowsInserted(QModelIndex,int,int)),this,SLOT(setHeaderForHHDC(QModelIndex,int,int)));
    connect(hangHoaXuatModel,
            SIGNAL(dataChanged(QModelIndex,QModelIndex,QVector<int>)),this,SLOT(xemThayDoi(QModelIndex,QModelIndex,QVector<int>)));
    //lay so hd va ngay
    query.exec("select * from HOA_DON_RA order by SO_HIEU desc");
    if(query.next()){
       ui->lineEditSohd->setText(QString::number(query.value("SO_HIEU").toInt()+1));
       ui->dateEditNgayXuat->setMinimumDate(query.value("NGAY_XUAT").toDate());
       reset();
    }
    connect(action_ChinhTonKho,SIGNAL(triggered(bool)),this,SLOT(suaTonKho()));
    connect(action_ChinhGiaBan,SIGNAL(triggered(bool)),this,SLOT(chinhGiaBan()));
    connect(action_xoaHHDC,SIGNAL(triggered(bool)),this,SLOT(xoaHangHoaDuocChon()));
    connect(mstComplete,SIGNAL(activated(QModelIndex)),this,SLOT(autoCompleted(QModelIndex)));
    connect(tenDnComplete,SIGNAL(activated(QModelIndex)),this,SLOT(autoCompleted(QModelIndex)));
}

XuatHoaDonForm::~XuatHoaDonForm()
{
    delete ui;
    delete hangHoaModel;
    delete hangHoaXuatModel;
    delete khachHangModel;
    delete xuatHoaDon;
    delete contextMenu;
    delete contextMenuHangHoaDC;
    delete action_ChinhTonKho;
    delete mstComplete;
    delete tenDnComplete;
    delete action_ChinhGiaBan;
    delete action_xoaHHDC;
}
QList<QStandardItem *> XuatHoaDonForm::append(const QString hangHoaID,
                                          const QString ten,
                                              const QString dvt,
                                             const QString soLuong,
                                             const QString donGia,
                                             const QString tongTien)
{
    QList<QStandardItem *> rowItems;
    rowItems << new QStandardItem(hangHoaID);//0
    rowItems << new QStandardItem(ten);//1
    rowItems << new QStandardItem(dvt);//2
    rowItems << new QStandardItem(soLuong);//3
    rowItems << new QStandardItem(donGia);//4
    rowItems << new QStandardItem(tongTien);//5
    return rowItems;
}

void XuatHoaDonForm::reset()
{
    ui->tableView->setEnabled(false);
    ui->tableViewHangHoa->setEnabled(false);
    hangHoaXuatModel->clear();
    ui->doubleSpinBoxTongTienHang->setValue(0);
    ui->pushButton_2->setEnabled(false);
    ui->lineEditMST->setText("");
    ui->lineEditTenDonVi->setText("");
    ui->lineEditTenKhachHan->setText("");
    ui->lineEditDiaChi->setText("");
}

void XuatHoaDonForm::on_tableViewHangHoa_doubleClicked(const QModelIndex &index)
{

    int currentIndex= Common::findIndex(hangHoaXuatModel,hangHoaModel->index(index.row(),2).data().toString(),0);

    if( currentIndex== -1){
        if(hangHoaModel->index(index.row(),3).data().toFloat() - 1 >=0){//kiem tra so luong+1 xem co lon hon so ton hien tai k
            hangHoaXuatModel->appendRow(append(hangHoaModel->index(index.row(),2).data().toString(),
                                               hangHoaModel->index(index.row(),0).data().toString(),
                                               hangHoaModel->index(index.row(),1).data().toString(),
                                               "1",//so luong
                                               hangHoaModel->index(index.row(),4).data().toString(),
                                               hangHoaModel->index(index.row(),4).data().toString()));
            ui->tableView->setEnabled(true);
            ui->pushButton_2->setEnabled(true);
        }else{
            QMessageBox::warning(this,"Vượt quá số lượng tồn","Bạn đã xuất quá số lượng còn lại trong kho: "+hangHoaModel->index(index.row(),3).data().toString(),QMessageBox::Ok,QMessageBox::NoButton);
        }


    }
    else{
        //cong so luong hang
        float soLuongHT;
        soLuongHT = hangHoaXuatModel->index(currentIndex,3).data().toFloat();
        soLuongHT+= 1;
        if(hangHoaModel->index(ui->tableViewHangHoa->currentIndex().row(),3).data().toDouble() - soLuongHT >=0){//kiem tra so luong+1 xem co lon hon so ton hien tai k
            hangHoaXuatModel->item(currentIndex,3)->setData(QVariant(soLuongHT),Qt::EditRole);
        }else{
            QMessageBox::warning(this,"Vượt quá số lượng tồn","Bạn đã xuất quá số lượng còn lại trong kho: "+hangHoaModel->index(index.row(),3).data().toString(),QMessageBox::Ok,QMessageBox::NoButton);
        }

        //hangHoaXuatModel->item(currentIndex,5)->setData(QVariant(soTienHt),Qt::EditRole);//so tien
    }

}

void XuatHoaDonForm::xemThayDoi(QModelIndex index, QModelIndex value, QVector<int> ww)
{
    Q_UNUSED(ww)
    //eo biet topleft la cai noi gi.

    if(index.column() == 3)//xac dinh vi tri cua don gia, va so luong. 3 = so luong, 4 = don gia
    {
        if(hangHoaModel->index(ui->tableViewHangHoa->currentIndex().row(),3).data().toDouble() - value.data().toFloat() >=0){//kiem tra so luong+1 xem co lon hon so ton hien tai k
            hangHoaXuatModel->item(value.row(),5)->setData(QString::number(round(value.data().toDouble()*hangHoaXuatModel->index(index.row(),4).data().toDouble()),'g',13),Qt::EditRole);//thay doi ton tien-> thay doi tong tien hang
        }else{
            QMessageBox::warning(this,"Vượt quá số lượng tồn","Bạn đã xuất quá số lượng còn lại trong kho: "+hangHoaModel->index(ui->tableViewHangHoa->currentIndex().row(),3).data().toString(),QMessageBox::Ok,QMessageBox::NoButton);
        }
    }else if(index.column() == 4){
        hangHoaXuatModel->item(value.row(),5)->setData(QString::number(round(value.data().toDouble()*hangHoaXuatModel->index(index.row()
                                                                                                            ,3).data().toFloat()),'g',13),Qt::EditRole);//thay doi ton tien-> thay doi tong tien hang
    }
}
void XuatHoaDonForm::tinhTongTienHangChuaTT()
{
    tongTienHang = 0;
    //chay vong lap for cho den het danh sach hang
    for (int i = 0; i < hangHoaXuatModel->rowCount(); ++i) {
            tongTienHang += hangHoaXuatModel->index(i,5).data().toDouble();
    }
    ui->doubleSpinBoxTongTienHang->setValue(tongTienHang);
    //ui->spinBoxSoTienDaTra->setValue(soTienDaTra);
}


void XuatHoaDonForm::on_doubleSpinBoxThue_valueChanged(double arg1)
{
    ui->doubleSpinBoxTienTHue->setValue(arg1*ui->doubleSpinBoxTongTienHang->value()/100);
}


void XuatHoaDonForm::on_tableViewKhachHang_doubleClicked(const QModelIndex &index)
{
    ui->tableViewHangHoa->setEnabled(true);
    xuatHoaDon->khid = khachHangModel->index(ui->tableViewKhachHang->currentIndex().row(),4).data().toString();
    ui->lineEditMST->setText(khachHangModel->index(index.row(),0).data().toString());
    ui->lineEditTenKhachHan->setText(khachHangModel->index(index.row(),1).data().toString());
    ui->lineEditTenDonVi->setText(khachHangModel->index(index.row(),2).data().toString());
    ui->lineEditDiaChi->setText(khachHangModel->index(index.row(),3).data().toString());
}

void XuatHoaDonForm::on_pushButton_2_clicked()
{
    if(hangHoaXuatModel->rowCount() >=0){
    QScopedPointer<ManageIndex> mnIndex;
    mnIndex.reset(new ManageIndex());
    //if()
    xuatHoaDon->hdid = mnIndex->getNextIndexCode("HOA_DON_RA","HR");
    //xuatHoaDon->khid = khachHangModel->index(ui->tableViewKhachHang->currentIndex().row(),4).data().toString();
    xuatHoaDon->ngayXuat = ui->dateEditNgayXuat->date().toString("yyyy-M-d");
    xuatHoaDon->mauSo = ui->lineEditMauSo->text();
    xuatHoaDon->kyHieu = ui->lineEditKyHieu->text();
    xuatHoaDon->soHieu = ui->lineEditSohd->text();//tang so hieu len 1 khi thanh cong
    xuatHoaDon->mst = ui->lineEditMST->text();
    xuatHoaDon->tenKhachHang = ui->lineEditTenKhachHan->text();
    xuatHoaDon->tenDonVi = ui->lineEditTenDonVi->text();
    xuatHoaDon->kieuThanhToan = ui->lineEditKieuThanhToan->text();
    xuatHoaDon->diaChi = ui->lineEditDiaChi->text();
    xuatHoaDon->tongTienHang = QString::number(ui->doubleSpinBoxTongTienHang->value(),'g',13);
    xuatHoaDon->thueSuat = ui->doubleSpinBoxThue->value();
    xuatHoaDon->tienThue = QString::number(ui->doubleSpinBoxTienTHue->value(),'g',13);
    xuatHoaDon->tongSoTien = QString::number(ui->doubleSpinBoxTongTien->value(),'g',13);
    xuatHoaDon->soTienBangChu = ui->lineEditTienBangChu->text();
    if(xuatHoaDon->themMoi(hangHoaXuatModel)){
        //tang so hieu len 1 khi thanh cong
        ui->lineEditSohd->setText(QString::number(xuatHoaDon->soHieu.toInt() +1));
        hangHoaModel->setQuery(hangHoaModel->query().lastQuery());
        xuatHoaDon->inHoaDon(xuatHoaDon->hdid);
        reset();

    }
    }else{
        QMessageBox::warning(this,"Không có hàng hóa xuất", "Bạn chưa chọn hàng hóa nào để xuất",QMessageBox::Ok,QMessageBox::NoButton);
    }
}
void XuatHoaDonForm::customMenuRequested(QPoint pos)
{
    contextMenu->popup(ui->tableViewHangHoa->viewport()->mapToGlobal(pos));
    ui->tableViewHangHoa->setCurrentIndex(ui->tableViewHangHoa->indexAt(pos));
}
void XuatHoaDonForm::customMenuHHDCRequested(QPoint pos)
{
    contextMenuHangHoaDC->popup(ui->tableView->viewport()->mapToGlobal(pos));
    ui->tableView->setCurrentIndex(ui->tableView->indexAt(pos));
}
void XuatHoaDonForm::suaTonKho()
{
    query.clear();
    bool ok;
    double d = QInputDialog::getDouble(this, tr("Thay đổi tồn kho"),
                                       tr("Chỉnh số tồn(Bạn không thể phục hồi lại được): "),
                                       hangHoaModel->index(ui->tableViewHangHoa->currentIndex().row(),3).data().toInt(), 0, 999999, 1, &ok);
    if (ok){
        query.prepare("update HANG_HOA set SO_DU = :soDu where HANGHOAID = :hhid");
        query.bindValue(":soDu", d);
        query.bindValue(":hhid",hangHoaModel->index(ui->tableViewHangHoa->currentIndex().row(),2).data().toString());
        if(query.exec()){
            if(query.numRowsAffected() != -1){//co it nhat 1 dong cap nhat
                hangHoaModel->setQuery(hangHoaModel->query().lastQuery());
            }
        }else{
            qDebug() << "Co kha nang loi o server or mang "<< query.lastError().text();
        }
    }

}

void XuatHoaDonForm::on_pushButtonLamMoi_clicked()
{
    khachHangModel->setQuery(khachHangModel->query().lastQuery());\
    hangHoaModel->setQuery(hangHoaModel->query().lastQuery());
}

void XuatHoaDonForm::on_doubleSpinBoxTongTienHang_valueChanged(double arg1)
{
    ui->doubleSpinBoxTienTHue->setValue(round(arg1*ui->doubleSpinBoxThue->value()/100));
}

void XuatHoaDonForm::on_doubleSpinBoxTienTHue_valueChanged(double arg1)
{
    ui->doubleSpinBoxTongTien->setValue(arg1+ui->doubleSpinBoxTongTienHang->value());
}

void XuatHoaDonForm::on_doubleSpinBoxTongTien_valueChanged(double arg1)
{
    ui->lineEditTienBangChu->setText(Common::docso(arg1));
    if(arg1 >=20000000){
        ui->lineEditKieuThanhToan->setText("TM/CK");
    }else{
        ui->lineEditKieuThanhToan->setText("TM");
    }
}

void XuatHoaDonForm::on_lineEdit_tenKhachHang_textChanged(const QString &arg1)
{
    //tim kiem theo ten khach hang
    khachHangModel->setQuery(tr(SELECT_STATEMENT_KHACH_HANG)+ " where TEN_DN like '%"+arg1+"%' or TEN_KH like '%"+arg1+"%' or MST like '%"+arg1+"%'");
}

void XuatHoaDonForm::on_lineEdit_tenKhachHang_returnPressed()
{
    qDebug() << "nhan asdsaasds";
}

void XuatHoaDonForm::autoCompleted(QModelIndex index)
{
    ui->tableViewHangHoa->setEnabled(true);
    xuatHoaDon->khid = khachHangModel->index(index.row(),4).data().toString();
    ui->lineEditMST->setText(khachHangModel->index(index.row(),0).data().toString());
    ui->lineEditTenKhachHan->setText(khachHangModel->index(index.row(),1).data().toString());
    ui->lineEditTenDonVi->setText(khachHangModel->index(index.row(),2).data().toString());
    ui->lineEditDiaChi->setText(khachHangModel->index(index.row(),3).data().toString());
}

void XuatHoaDonForm::on_lineEditTenHangHoa_textChanged(const QString &arg1)
{
    hangHoaModel->setQuery(tr(SELECT_STATEMENT_HANG_HOA)+ " where TEN_HANG_HOA like '%"+arg1+"%' or HANGHOAID like '%"+arg1+"%'");

}

void XuatHoaDonForm::chinhGiaBan()
{
    query.clear();
    bool ok;
    double d = QInputDialog::getDouble(this, tr("Thay đổi tồn kho"),
                                       tr("Chỉnh giá bán ra: "),
                                       hangHoaModel->index(ui->tableViewHangHoa->currentIndex().row(),4).data().toInt(), 0, 999999, 1, &ok);
    if (ok){
        query.prepare("update HANG_HOA set GIA_BAN = :giaBan where HANGHOAID = :hhid");
        query.bindValue(":giaBan", d);
        query.bindValue(":hhid",hangHoaModel->index(ui->tableViewHangHoa->currentIndex().row(),2).data().toString());
        if(query.exec()){
            if(query.numRowsAffected() != -1){//co it nhat 1 dong cap nhat
                hangHoaModel->setQuery(hangHoaModel->query().lastQuery());
            }
        }else{
            qDebug() << "Co kha nang loi o server or mang "<< query.lastError().text();
        }
    }
}

void XuatHoaDonForm::xoaHangHoaDuocChon()
{
    if(hangHoaXuatModel->removeRow(ui->tableView->currentIndex().row())){

        //hangHoaModel->setQuery(hangHoaModel->query().lastQuery());

    }
}

void XuatHoaDonForm::setHeaderForHHDC(QModelIndex index,int x ,int y)
{
    Q_UNUSED(index)
    if(x==0){
        qDebug()<<"co setHeader";
    hangHoaXuatModel->setHeaderData(0,Qt::Horizontal,"Mã hàng");
    hangHoaXuatModel->setHeaderData(1,Qt::Horizontal,"Tên hàng");
    hangHoaXuatModel->setHeaderData(2,Qt::Horizontal,"ĐVT");
    hangHoaXuatModel->setHeaderData(3,Qt::Horizontal,"Sô lượng");
    hangHoaXuatModel->setHeaderData(4,Qt::Horizontal,"Giá bán");
    hangHoaXuatModel->setHeaderData(5,Qt::Horizontal,"Tổng tiền");
    }else{
        qDebug() <<"Khong set header"<<x<<y;
    }
}
