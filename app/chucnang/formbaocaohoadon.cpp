#include "formbaocaohoadon.h"
#include "ui_formbaocaohoadon.h"

FormBaoCaoHoaDon::FormBaoCaoHoaDon(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::FormBaoCaoHoaDon)
{
    ui->setupUi(this);
    chiTietModel = new QSqlQueryModel();
    hoadonModel = new QSqlQueryModel();
    ui->treeViewChiTiet->setModel(chiTietModel);
    ui->treeViewHoaDon->setModel(hoadonModel);
    hoadonModel->setQuery("select hoadonid from HOA_DON");
}

FormBaoCaoHoaDon::~FormBaoCaoHoaDon()
{
    delete ui;
    delete hoadonModel;
    delete chiTietModel;
}

void FormBaoCaoHoaDon::on_treeViewHoaDon_clicked(const QModelIndex &index)
{
    QString hoaDonId;
    hoaDonId = hoadonModel->index(index.row(),0).data().toString();
    chiTietModel->setQuery("SELECT thuoc.TEN_THUOC,chi_tiet_hoa_don.SO_LUONG,chi_tiet_hoa_don.DON_GIA FROM chi_tiet_hoa_don left JOIN thuoc ON thuoc.THUOCID = chi_tiet_hoa_don.THUOCID where HOADONID = '"+hoaDonId+"'");
    query.exec("select tai_khoan.TEN_NV,TONG_SO_TIEN,khach_hang.TEN_KHACH_HANG from HOA_DON "
               "left join TAI_KHOAN on TAI_KHOAN.USERNAME = HOA_DON.USERNAME "
               "left join khach_hang on khach_hang.KHACHHANGID = hoa_don.KHACHHANGID where HOADONID = '"+hoaDonId+"'");
    if(query.next()){
        ui->lineEditTongTien->setText(query.value(1).toString());
        ui->lineEditTenKh->setText(query.value(2).toString());
        ui->lineEditTenNV->setText(query.value(0).toString());
    }
}

void FormBaoCaoHoaDon::on_lineEditTim_textChanged(const QString &arg1)
{
    hoadonModel->setQuery("select hoadonid from HOA_DON where HOADONID like '%"+arg1+"%'");
}
