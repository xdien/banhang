#ifndef DIALOGKHACHHANG_H
#define DIALOGKHACHHANG_H

#include <QWidget>
#include "utils/khachhang.h"
#include <Objects/okhachhang.h>
#include <QSqlQueryModel>
#include <QSqlTableModel>
#include <QMenu>
#include <QSqlRelation>
#include <QMessageBox>
#include "chucnang/formnhomkhachhang.h"
namespace Ui {
class DialogKhachHang;
}

class DialogKhachHang : public QDialog
{
    Q_OBJECT

public:
    explicit DialogKhachHang(QWidget *parent = 0);
    ~DialogKhachHang();

private slots:
    void on_pushButton_clicked();

    void on_toolButtonThemNhom_clicked();
    void lamMoiCb();

    void on_checkBox_clicked(bool checked);

    void on_pushButton_Luu_clicked();
    void customMenuRequested(QPoint pos);
    void xoaKhachHang();
private:
    Ui::DialogKhachHang *ui;
    KhachHang *khachHang;
    QSqlQueryModel *cbNhomKhachhangModel;
    QSqlRelationalTableModel *khachHangModel;
    ManageIndex *taoIndex;
    QMenu *contextMenu;
    QAction *action_Xoa;
};

#endif // DialogKhachHang_H
