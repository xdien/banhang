#ifndef FORMBAOCAOHOADON_H
#define FORMBAOCAOHOADON_H

#include <QWidget>
#include <QSqlQueryModel>
#include <QSqlQuery>

namespace Ui {
class FormBaoCaoHoaDon;
}

class FormBaoCaoHoaDon : public QWidget
{
    Q_OBJECT

public:
    explicit FormBaoCaoHoaDon(QWidget *parent = 0);
    ~FormBaoCaoHoaDon();

private slots:
    void on_treeViewHoaDon_clicked(const QModelIndex &index);

    void on_lineEditTim_textChanged(const QString &arg1);

private:
    QSqlQueryModel *hoadonModel, *chiTietModel;
    Ui::FormBaoCaoHoaDon *ui;
    QSqlQuery query;
};

#endif // FORMBAOCAOHOADON_H
