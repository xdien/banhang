#include "formphieuchi.h"
#include "ui_formphieuchi.h"

FormPhieuChi::FormPhieuChi(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::FormPhieuChi)
{
    ui->setupUi(this);
    tableModelPhieuChi = new QSqlTableModel();
    phieuChi = new PhieuChi();
    taiKhoan = new TaiKhoan();
    ui->tableView->setModel(tableModelPhieuChi);
    tableModelPhieuChi->setTable("PHIEU_CHI");
    tableModelPhieuChi->select();
    setWindowTitle("Chi tiết phiếu chi ["+QString::fromLatin1(metaObject()->className())+"]");
}

FormPhieuChi::~FormPhieuChi()
{
    delete ui;
    delete tableModelPhieuChi;
    delete taiKhoan;
}

void FormPhieuChi::on_pushButton_clicked()
{
    QSharedPointer<OPhieuChi> Oncc(new OPhieuChi());
    Oncc.data()->username = taiKhoan->layIDDangNhapHienTai();//lay thong tin nguoi dung hien tai
    Oncc.data()->soTien = ui->lineEditSoTIen->text();
    if(phieuChi->themPhieuChi(Oncc.data())){
        tableModelPhieuChi->select();//refesh
    }
}
