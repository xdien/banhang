/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     6/1/2016 3:12:56 PM                          */
/*==============================================================*/


drop table if exists BANG_GIA;

drop table if exists CHI_TIET_RA;

drop table if exists CHI_TIET_VAO;

drop table if exists DON_VI_TINH;

drop table if exists HANG_HOA;

drop table if exists HOA_DON_RA;

drop table if exists HOA_DON_VAO;

drop table if exists KHACH_HANG;

drop table if exists NHOM_HANG_HOA;

drop table if exists NHOM_KHACH_HANG;

drop table if exists PHIEU_CHI;

drop table if exists QUYEN;

drop table if exists TAI_KHOAN;

/*==============================================================*/
/* Table: BANG_GIA                                              */
/*==============================================================*/
create table BANG_GIA
(
   GIAID                varchar(15) not null,
   HOANGHOAID           varchar(12) not null,
   GIA_HANG_HOA         float,
   NGAY_AP_DUNG         date,
   primary key (GIAID)
);

alter table BANG_GIA comment 'Gia thu?c l?y ng�y cu?i c�ng l�m gi� hi?n t?i
';

/*==============================================================*/
/* Table: CHI_TIET_RA                                           */
/*==============================================================*/
create table CHI_TIET_RA
(
   HOADONRAID           varchar(15) not null,
   HOANGHOAID           varchar(12) not null,
   SO_LUONG             int,
   DON_GIA_XUAT         bigint,
   primary key (HOADONRAID, HOANGHOAID)
);

/*==============================================================*/
/* Table: CHI_TIET_VAO                                          */
/*==============================================================*/
create table CHI_TIET_VAO
(
   HOADONVAOID          char(12) not null,
   HOANGHOAID           varchar(12) not null,
   SO_LUONG             int,
   DON_GIA_XUAT         bigint,
   primary key (HOADONVAOID, HOANGHOAID)
);

/*==============================================================*/
/* Table: DON_VI_TINH                                           */
/*==============================================================*/
create table DON_VI_TINH
(
   DVTID                char(12) not null,
   DON_VI               varchar(32),
   primary key (DVTID)
);

/*==============================================================*/
/* Table: HANG_HOA                                              */
/*==============================================================*/
create table HANG_HOA
(
   HOANGHOAID           varchar(12) not null,
   DVTID                char(12) not null,
   NHOM_HANG_HOA_ID     varchar(30) not null,
   TEN_HANG_HOA         varchar(35),
   DIEN_GIAI            text,
   SO_DU                int,
   primary key (HOANGHOAID)
);

/*==============================================================*/
/* Table: HOA_DON_RA                                            */
/*==============================================================*/
create table HOA_DON_RA
(
   HOADONRAID           varchar(15) not null,
   USERNAME             varchar(32) not null,
   KHACHHANGID          varchar(15) not null,
   NGAY_LAP             timestamp default CURRENT_TIMESTAMP,
   TONG_SO_TIEN         bigint,
   KY_HIEU              varchar(10) comment 'AC/15P
            CT/15T',
   SO_HIEU              int comment 'sstt to hoa don
            ',
   MAU_SO               varchar(11) comment '01GTKT3/001
            01GTKT3/002
            01GTKT3/003
            ...',
   primary key (HOADONRAID)
);

/*==============================================================*/
/* Table: HOA_DON_VAO                                           */
/*==============================================================*/
create table HOA_DON_VAO
(
   HOADONVAOID          char(12) not null,
   USERNAME             varchar(32) not null,
   TONG_SO_TIEN         bigint,
   MAU_SO               varchar(11) comment '01GTKT3/001
            01GTKT3/002
            01GTKT3/003
            ...',
   KY_HIEU              varchar(10) comment 'AC/15P
            CT/15T',
   SO_HIEU              int comment 'sstt to hoa don
            ',
   NGAY_LAP_HDV         date comment 'Ngay lap do ben ban cung cap nen khong the luu chinh xac thoi gian
            ',
   THOI_GIAN_TAO        timestamp default  CURRENT_TIMESTAMP,
   primary key (HOADONVAOID)
);

alter table HOA_DON_VAO comment 'H?i khi nh?p h�ng v?(mua t?i ch? du?c) th� mua l? m?t l�c c�';

/*==============================================================*/
/* Table: KHACH_HANG                                            */
/*==============================================================*/
create table KHACH_HANG
(
   KHACHHANGID          varchar(15) not null,
   NHOMID               varchar(15) not null,
   MST                  char(13),
   TEN_KH               varchar(33),
   DIA_CHI_KH           text,
   TEN_DN               text,
   primary key (KHACHHANGID)
);

/*==============================================================*/
/* Table: NHOM_HANG_HOA                                         */
/*==============================================================*/
create table NHOM_HANG_HOA
(
   NHOM_HANG_HOA_ID     varchar(30) not null,
   NHO_NHOM_HANG_HOA_ID varchar(30),
   MO_TA                varchar(50),
   primary key (NHOM_HANG_HOA_ID)
);

/*==============================================================*/
/* Table: NHOM_KHACH_HANG                                       */
/*==============================================================*/
create table NHOM_KHACH_HANG
(
   NHOMID               varchar(15) not null,
   MO_TA_NHOM           text,
   primary key (NHOMID)
);

/*==============================================================*/
/* Table: PHIEU_CHI                                             */
/*==============================================================*/
create table PHIEU_CHI
(
   PHIEUCHIID           varchar(15) not null,
   USERNAME             varchar(32) not null,
   NGAY_LAP_PC          timestamp,
   SO_TIEN              bigint,
   GHI_CHU              text,
   primary key (PHIEUCHIID)
);

/*==============================================================*/
/* Table: QUYEN                                                 */
/*==============================================================*/
create table QUYEN
(
   FORMID               varchar(45) not null,
   MO_TA                varchar(50),
   XEM                  bool,
   CAP_NHAT             bool,
   primary key (FORMID)
);

/*==============================================================*/
/* Table: TAI_KHOAN                                             */
/*==============================================================*/
create table TAI_KHOAN
(
   USERNAME             varchar(32) not null,
   PASSWORD             varchar(30),
   TEN_NV               varchar(32),
   GIOI_TINH            bool,
   DIA_CHI_NV           text,
   COQUYEN              text,
   primary key (USERNAME)
);

alter table BANG_GIA add constraint FK_AP_DUNG_GIA foreign key (HOANGHOAID)
      references HANG_HOA (HOANGHOAID) on delete restrict on update restrict;

alter table CHI_TIET_RA add constraint FK_CHI_TIET_THUOC foreign key (HOANGHOAID)
      references HANG_HOA (HOANGHOAID) on delete restrict on update restrict;

alter table CHI_TIET_RA add constraint FK_HOA_DON_CHI_TIET foreign key (HOADONRAID)
      references HOA_DON_RA (HOADONRAID) on delete restrict on update restrict;

alter table CHI_TIET_VAO add constraint FK_NHAP foreign key (HOANGHOAID)
      references HANG_HOA (HOANGHOAID) on delete restrict on update restrict;

alter table CHI_TIET_VAO add constraint FK_PHIEU_NHAP_CHI_TIET foreign key (HOADONVAOID)
      references HOA_DON_VAO (HOADONVAOID) on delete restrict on update restrict;

alter table HANG_HOA add constraint FK_RELATIONSHIP_14 foreign key (DVTID)
      references DON_VI_TINH (DVTID) on delete restrict on update restrict;

alter table HANG_HOA add constraint FK_RELATIONSHIP_16 foreign key (NHOM_HANG_HOA_ID)
      references NHOM_HANG_HOA (NHOM_HANG_HOA_ID) on delete restrict on update restrict;

alter table HOA_DON_RA add constraint FK_LAP_HOA_DON foreign key (USERNAME)
      references TAI_KHOAN (USERNAME) on delete restrict on update restrict;

alter table HOA_DON_RA add constraint FK_MUA_THUOC foreign key (KHACHHANGID)
      references KHACH_HANG (KHACHHANGID) on delete restrict on update restrict;

alter table HOA_DON_VAO add constraint FK_DOI_CHIEU foreign key (USERNAME)
      references TAI_KHOAN (USERNAME) on delete restrict on update restrict;

alter table KHACH_HANG add constraint FK_THUOC_NHOM_KHACH foreign key (NHOMID)
      references NHOM_KHACH_HANG (NHOMID) on delete restrict on update restrict;

alter table NHOM_HANG_HOA add constraint FK_RELATIONSHIP_15 foreign key (NHO_NHOM_HANG_HOA_ID)
      references NHOM_HANG_HOA (NHOM_HANG_HOA_ID) on delete restrict on update restrict;

alter table PHIEU_CHI add constraint FK_LAP_PHIEU_CHI foreign key (USERNAME)
      references TAI_KHOAN (USERNAME) on delete restrict on update restrict;

