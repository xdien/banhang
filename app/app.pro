#-------------------------------------------------
#
# Project created by QtCreator 2015-01-23T20:15:57
#
#-------------------------------------------------
include(../common.pri)
QT       += core gui sql xml printsupport qml quick xlsx

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets printsupport
TARGET = banhang
TEMPLATE = app
QTPLUGIN += qwindows
INCLUDEPATH += $$PWD/../include
DEPENDPATH  += $$PWD/../include

CONFIG(release, debug|release){
    LIBS += -L$$PWD/../build -lCryptoPPLib -lQSCore -lqaivlib -lCoreApp -lNhanSuCore -lKinhDoanhCore -lDichVuCore -llimereport
} else {
    LIBS += -L$$PWD/../build -lCryptoPPLibd -lQSCored -lqaivlibd -lCoreAppd -lNhanSuCored -lKinhDoanhCored -lDichVuCored -llimereportd
}
win32 {
    EXTRA_FILES ~= s,/,\\,g
    BUILD_DIR ~= s,/,\\,g
    DESTDIR = $${DEST_LIBS}
    #DEST_DIR ~= s,/,\\,g
    DEST_INCLUDE_DIR ~= s,/,\\,g
    LIBS += -L$${DEST_LIBS}
    target.path = $${DEST_DIR}
    #for(FILE,EXTRA_FILES){
    #    QMAKE_POST_LINK += $$QMAKE_COPY \"$$FILE\" \"$${DEST_INCLUDE_DIR}\" $$escape_expand(\\n\\t)
    #}
    #QMAKE_POST_LINK += $$QMAKE_COPY_DIR \"$${DEST_INCLUDE_DIR}\" \"$${DEST_DIR}\"
    INSTALLS = target
}
linux{
    #Link share lib to ../lib rpath
    QMAKE_LFLAGS += -Wl,--rpath=\\\$\$ORIGIN
    QMAKE_LFLAGS += -Wl,--rpath=\\\$\$ORIGIN/lib
    QMAKE_LFLAGS += -Wl,--rpath=\\\$\$ORIGIN/../lib
    QMAKE_LFLAGS_RPATH += #. .. ./libs
    DESTDIR = $${DEST_LIBS}
    DEST_DIR = $$DESTDIR/include
    LIBS += -L$${DEST_LIBS}
    target.path = $${DEST_DIR}
    INSTALLS = target
}
macx:{
#khi build cho macos phai copy thu vien con thieu vao /usr/local/lib, va copy thu vien qt vao /Framwork/
    CONFIG  += app_bundle no_qt_rpath relative_qt_rpath
#    ICON = isuzu_logo.ico.icns
    DESTDIR = $${DEST_LIBS}
     QMAKE_RPATHDIR = @executable_path/../Frameworks
    DEST_DIR = $$DESTDIR/include
    target.path = $${DEST_DIR}
    INSTALLS = target
}


# ket thuc copy
SOURCES += main.cpp\
        mainwindow.cpp \
    dialog_setting.cpp \
    config/config_sqlconnect.cpp \
    login.cpp \
    about.cpp \
    Objects/sqlmanage.cpp \
    formbanthuoc.cpp \
    Objects/nsx.cpp \
    utils/qlnhasanxuat.cpp \
    utils/manageindex.cpp \
    Objects/ncc.cpp \
    utils/nhacungcap.cpp \
    Objects/nkh.cpp \
    chucnang/formnhomkhachhang.cpp \
    utils/nhomkhachhang.cpp \
    Objects/ogia.cpp \
    utils/gia.cpp \
    chucnang/formgia.cpp \
    Objects/ddh.cpp \
    utils/dondathang.cpp \
    chucnang/formdathang.cpp \
    Objects/otaikhoan.cpp \
    Objects/oquyen.cpp \
    utils/quyen.cpp \
    chucnang/formquyen.cpp \
    utils/taikhoan.cpp \
    chucnang/formtaikhoan.cpp \
    Objects/ophieuchi.cpp \
    utils/phieuchi.cpp \
    chucnang/formphieuchi.cpp \
    Objects/ophieunhap.cpp \
    utils/nhaphang.cpp \
    chucnang/formphieunhap.cpp \
    Objects/ochitietphieunhap.cpp \
    utils/chitietphieunhap.cpp \
    chucnang/formchitietphieunhap.cpp \
    Objects/olothuoc.cpp \
    utils/lothuoc.cpp \
    Objects/othuoc.cpp \
    utils/thuoc.cpp \
    Objects/okhachhang.cpp \
    utils/khachhang.cpp \
    Objects/ohoadon.cpp \
    Objects/ohoadonchitiet.cpp \
    utils/hoadon.cpp \
    utils/chitiethoadon.cpp \
    chucnang/formphanquyen.cpp \
    Objects/obaocaohoadon.cpp \
    utils/baocaohoadon.cpp \
    chucnang/formbaocaohoadon.cpp \
    chucnang/formdauvao.cpp \
    utils/hoadonvao.cpp \
    Objects/ochitiethoadonvao.cpp \
    utils/chitiethoadonvao.cpp \
    Objects/onhomhanghoa.cpp \
    utils/nhomhanghoa.cpp \
    chucnang/formnhomhanghoa.cpp \
    utils/donvitinh.cpp \
    chucnang/formhanghoa.cpp \
    Objects/ohanghoa.cpp \
    utils/hanghoa.cpp \
    Objects/odauvao.cpp \
    Objects/ohoadonvao.cpp \
    Objects/ohoadonra.cpp \
    utils/hoadonra.cpp \
    Objects/ochitiethoadonra.cpp \
    utils/chitiethoadonra.cpp \
    chucnang/formtonkho.cpp \
    chucnang/bcdaura.cpp \
    chucnang/formduyetkh.cpp \
    chucnang/bctonkho.cpp \
    chucnang/chitiet/ctmottonkho.cpp \
    chucnang/xuathoadonform.cpp \
    utils/xuathoadon.cpp \
    config/common.cpp \
    Objects/xuathangmodel.cpp \
    chucnang/dialogkhachhang.cpp

HEADERS  += mainwindow.h \
    dialog_setting.h \
    config/config_sqlconnect.h \
    login.h \
    about.h \
    Objects/sqlmanage.h \
    formbanthuoc.h \
    Objects/nsx.h \
    utils/qlnhasanxuat.h \
    utils/manageindex.h \
    Objects/ncc.h \
    utils/nhacungcap.h \
    Objects/nkh.h \
    chucnang/formnhomkhachhang.h \
    utils/nhomkhachhang.h \
    Objects/ogia.h \
    utils/gia.h \
    chucnang/formgia.h \
    Objects/ddh.h \
    utils/dondathang.h \
    chucnang/formdathang.h \
    Objects/otaikhoan.h \
    Objects/oquyen.h \
    utils/quyen.h \
    chucnang/formquyen.h \
    utils/taikhoan.h \
    chucnang/formtaikhoan.h \
    Objects/ophieuchi.h \
    utils/phieuchi.h \
    chucnang/formphieuchi.h \
    Objects/ophieunhap.h \
    utils/nhaphang.h \
    chucnang/formphieunhap.h \
    Objects/ochitietphieunhap.h \
    utils/chitietphieunhap.h \
    chucnang/formchitietphieunhap.h \
    Objects/olothuoc.h \
    utils/lothuoc.h \
    Objects/othuoc.h \
    utils/thuoc.h \
    Objects/okhachhang.h \
    utils/khachhang.h \
    Objects/ohoadon.h \
    Objects/ohoadonchitiet.h \
    utils/hoadon.h \
    utils/chitiethoadon.h \
    chucnang/formphanquyen.h \
    Objects/obaocaohoadon.h \
    utils/baocaohoadon.h \
    chucnang/formbaocaohoadon.h \
    chucnang/formdauvao.h \
    utils/hoadonvao.h \
    Objects/ochitiethoadonvao.h \
    utils/chitiethoadonvao.h \
    Objects/onhomhanghoa.h \
    utils/nhomhanghoa.h \
    chucnang/formnhomhanghoa.h \
    utils/donvitinh.h \
    chucnang/formhanghoa.h \
    Objects/ohanghoa.h \
    utils/hanghoa.h \
    Objects/odauvao.h \
    Objects/ohoadonvao.h \
    Objects/ohoadonra.h \
    utils/hoadonra.h \
    Objects/ochitiethoadonra.h \
    utils/chitiethoadonra.h \
    chucnang/formtonkho.h \
    chucnang/bcdaura.h \
    chucnang/formduyetkh.h \
    chucnang/bctonkho.h \
    chucnang/chitiet/ctmottonkho.h \
    chucnang/xuathoadonform.h \
    utils/xuathoadon.h \
    config/common.h \
    Objects/xuathangmodel.h \
    chucnang/dialogkhachhang.h

FORMS    += mainwindow.ui \
    dialog_setting.ui \
    login.ui \
    about.ui \
    formbanthuoc.ui \
    chucnang/formnhomkhachhang.ui \
    chucnang/formgia.ui \
    chucnang/formdathang.ui \
    chucnang/formquyen.ui \
    chucnang/formtaikhoan.ui \
    chucnang/formphieuchi.ui \
    chucnang/formphieunhap.ui \
    chucnang/formchitietphieunhap.ui \
    chucnang/formphanquyen.ui \
    chucnang/formbaocaohoadon.ui \
    chucnang/formdauvao.ui \
    chucnang/formnhomhanghoa.ui \
    chucnang/formhanghoa.ui \
    chucnang/formtonkho.ui \
    chucnang/bcdaura.ui \
    chucnang/formduyetkh.ui \
    chucnang/bctonkho.ui \
    chucnang/chitiet/ctmottonkho.ui \
    chucnang/xuathoadonform.ui \
    chucnang/dialogkhachhang.ui

DISTFILES +=

RESOURCES += \
    qml/qml.qrc
